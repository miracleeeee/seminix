#!/usr/bin/env python3
#
# Copyright (c) 2017 Intel Corporation.
#
# SPDX-License-Identifier: Apache-2.0
#

import argparse
import sys
import re


def gen_symbol_asm(input_file, output_file, symbol_list, prefix):
    output_file.write("""/* THIS FILE IS AUTO GENERATED.  PLEASE DO NOT EDIT.
 *
 */
""")
    text_line = input_file.readline()
    while text_line:
        line_list = text_line.split(' ')
        for symbol in symbol_list:
            if symbol + '\n' == line_list[2]:
                outputsymbol = prefix + symbol
                outputaddress = "0x" + line_list[0]
                output_file.write("\n.global %s;\n.align 3;\n%s:\n    .quad %s\n" % (outputsymbol, outputsymbol, outputaddress))
        text_line = input_file.readline()

    return 0


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument(
        "-i",
        "--input",
        required=True,
        help="Input object file")

    parser.add_argument(
        "-o",
        "--output",
        required=True,
        help="Output header file")

    parser.add_argument(
        "-s",
        "--sym",
        required=True,
        help="Parse symbol name")

    parser.add_argument(
        "-a",
        "--alias",
        required=True,
        help="Parse symbol output name")

    args = parser.parse_args()

    input_file = open(args.input, 'r')
    output_file = open(args.output, 'w')
    symbol = args.sym
    prefix = args.alias

    sym_list = symbol.split(';')

    ret = gen_symbol_asm(input_file, output_file, sym_list, prefix)

    sys.exit(0)
