# SPDX-License-Identifier: Apache-2.0

set(SEMINIX_KERNEL_DIR "${CMAKE_CURRENT_LIST_DIR}" CACHE STRING "")
set(APPLICATION_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR} CACHE PATH "Application Source Directory")
set(APPLICATION_BINARY_DIR ${CMAKE_CURRENT_BINARY_DIR} CACHE PATH "Application Binary Directory")
mark_as_advanced(SEMINIX_KERNEL_DIR APPLICATION_SOURCE_DIR APPLICATION_BINARY_DIR)

function(seminix_import_kernel)
  add_subdirectory(${SEMINIX_KERNEL_DIR} ${CMAKE_BINARY_DIR}/seminix)
endfunction()

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(seminix DEFAULT_MSG SEMINIX_KERNEL_DIR APPLICATION_SOURCE_DIR APPLICATION_BINARY_DIR)
