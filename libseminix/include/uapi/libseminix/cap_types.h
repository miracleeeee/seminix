#ifndef UAPI_LIBSEMINIX_CAP_TYPES_H
#define UAPI_LIBSEMINIX_CAP_TYPES_H

enum {
    cap_null_cap = 0,
    cap_rlimit_cap,
    cap_cnode_cap,
    cap_thread_cap,
    cap_devctl_cap,
    cap_device_cap,
    cap_devirq_cap,
    cap_deadline_cap,
    cap_vspace_cap,
    cap_endpoint_cap,
    cap_sigctl_cap,
    cap_sysctl_cap,
    cap_frame_cap,      /* 由用户创建的共享 frame/ 设备导出的 frame 等特殊区域, 不可以被 vspace op 修改/复制 */
    cap_page_cap,       /* 由 vspace 引用虚拟地址空间的使用量 */
    cap_count_max,
};

static inline bool seminix_cap_type_invalid(int type)
{
    return unlikely(type < cap_rlimit_cap || type > cap_page_cap);
}

#endif /* !UAPI_LIBSEMINIX_CAP_TYPES_H */
