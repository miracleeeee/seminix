#ifndef UAPI_LIBSEMINIX_TCB_TYPES_H
#define UAPI_LIBSEMINIX_TCB_TYPES_H

#define TCB_CONFIG_CNODE     1
#define TCB_CONFIG_VSPACE    2
#define TCB_CONFIG_EPFAULT   4
#define TCB_CONFIG_IPCBUF    8
#define TCB_CONFIG_RLIMIT    16

typedef struct tcb_config {
    int cnode;
    int vspace;
    int epfault;
    int ipcbuf;
    int rlimit;
} tcb_config_t;

#define TCB_STATUS_SUSPEND  1   /* suspend */
#define TCB_STATUS_RUNNING  2   /* running */
#define TCB_STATUS_WAIT     3   /* wait/sleep... */
#define TCB_STATUS_DEAD     4   /* crash */
#define TCB_STATUS_KILL     5   /* kill */
#define TCB_STATUS_EXIT     6   /* exit */
#define TCB_STATUS_NEW      7   /* new */

#define TCB_CONTROL_SUSPEND     1   /* running -> stoped */
#define TCB_CONTROL_RESUME      2   /* stoped -> running */
#define TCB_CONTROL_GETSTATUS   3
#define TCB_CONTROL_SETAFFINITY 4
#define TCB_CONTROL_GETAFFINITY 5
#define TCB_CONTROL_KILL        6
#define TCB_CONTROL_GETTID      7

/* tcb wake up flags */
#define TCB_WAKEUP_NOPC          1
#define TCB_WAKEUP_SETTLS        2
#define TCB_WAKEUP_PARENT_GETTID 4
#define TCB_WAKEUP_CHILD_SETTID  8

#endif /* !UAPI_LIBSEMINIX_TCB_TYPES_H */
