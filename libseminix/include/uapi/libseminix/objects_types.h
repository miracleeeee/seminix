/*
 * Copyright 2020, Data61, CSIRO (ABN 41 687 119 230)
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */
#ifndef UAPI_LIBSEMINIX_OBJECTS_TYPES_H
#define UAPI_LIBSEMINIX_OBJECTS_TYPES_H

#include "deadline_types.h"

typedef enum seminix_object_type {
    seminix_ObjectNone = 0,
    seminix_RlimitObject,
    seminix_CnodeObject,
    seminix_TcbObject,
    seminix_DeadlineObject,
    seminix_VspaceObject,
    seminix_EndpointObject,
    seminix_FrameObject,
    seminix_ObjectTypeCount,
} seminix_object_type_t;

static inline bool seminix_object_type_invalid(seminix_object_type_t type)
{
    return (type < seminix_RlimitObject) || (type > seminix_FrameObject);
}

typedef struct seminix_object {
    seminix_object_type_t type;
    union {
        unsigned long type_data;
        struct {
            int count;
        } cnode;
        struct deadline_sched_param deadline;
        struct {
            int order;
            int count;
        } frame;
        struct {
            unsigned long badge;
        } endpoint;
    };
} seminix_object_t;

#endif /* !UAPI_LIBSEMINIX_OBJECTS_TYPES_H */
