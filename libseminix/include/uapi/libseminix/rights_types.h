#ifndef UAPI_LIBSEMINIX_RIGHTS_TYPES_H
#define UAPI_LIBSEMINIX_RIGHTS_TYPES_H

/*
 * bit0: create
 * bit1: dup
 * bit2: move
 * bit4: send
 * bit5: recv
 * bit6: write
 */
static inline __attribute_const__
int seminix_cap_rights_new(bool create, bool dup,
    bool move, bool send, bool recv, bool write)
{
    int rights = 0;

    assert((create & ~0x1u) == 0);
    assert((dup & ~0x1u) == 0);
    assert((move & ~0x1u) == 0);
    assert((send & ~0x1u) == 0);
    assert((recv & ~0x1u) == 0);
    assert((write & ~0x1u) == 0);

    rights |= create |
              dup << 1 |
              move << 2 |
              send << 4 |
              recv << 5 |
              write << 6;

    return rights;
}

static inline bool seminix_cap_rights_allow_create(int rights)
{
    return rights & (0x1 << 0);
}

static inline bool seminix_cap_rights_allow_dup(int rights)
{
    return rights & (0x1 << 1);
}

static inline bool seminix_cap_rights_allow_move(int rights)
{
    return rights & (0x1 << 2);
}

static inline bool seminix_cap_rights_allow_send(int rights)
{
    return rights & (0x1 << 4);
}

static inline bool seminix_cap_rights_allow_recv(int rights)
{
    return rights & (0x1 << 5);
}

static inline bool seminix_cap_rights_allow_write(int rights)
{
    return rights & (0x1 << 6);
}

#define seminix_all_rights    seminix_cap_rights_new(1, 1, 1, 1, 1, 1)
#define seminix_can_create    seminix_cap_rights_new(1, 0, 0, 0, 0, 0)
#define seminix_can_dup       seminix_cap_rights_new(0, 1, 0, 0, 0, 0)
#define seminix_can_move      seminix_cap_rights_new(0, 0, 1, 0, 0, 0)
#define seminix_can_send      seminix_cap_rights_new(0, 0, 0, 1, 0, 0)
#define seminix_can_recv      seminix_cap_rights_new(0, 0, 0, 0, 1, 0)
#define seminix_no_rights     seminix_cap_rights_new(0, 0, 0, 0, 0, 0)

#endif /* !UAPI_LIBSEMINIX_RIGHTS_TYPES_H */
