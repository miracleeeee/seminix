#ifndef UAPI_LIBSEMINIX_SCHED_TYPES_H
#define UAPI_LIBSEMINIX_SCHED_TYPES_H

/*
 * Scheduling policies
 */
#define SEMINIX_SCHED_IDLE      0
#define SEMINIX_SCHED_BATCH		1
#define SEMINIX_SCHED_NORMAL    2
#define SEMINIX_SCHED_RR	    3
#define SEMINIX_SCHED_FIFO		4
#define SEMINIX_SCHED_DEADLINE  5

typedef struct tcb_sched_params {
    int priority;
    int mcpriority;
    int policy;
} tcb_sched_params_t;

#define SCHED_PARAMS_SETPRIORITY   1
#define SCHED_PARAMS_SETMCPRIORITY 2
#define SCHED_PARAMS_SETALL        3
#define SCHED_PARAMS_GET           4

#endif /* !UAPI_LIBSEMINIX_SCHED_TYPES_H */
