#ifndef UAPI_lIBSEMINIX_ERRNO_TYPES_H
#define UAPI_lIBSEMINIX_ERRNO_TYPES_H

#define SERRNO_NONE          0
#define SERRNO_EINVAL        1
#define SERRNO_EILLEGAL      2
#define SERRNO_EBUSY         3
#define SERRNO_ENOMEM        4
#define SERRNO_EOVERFLOW     5
#define SERRNO_EREMOVING     6
#define SERRNO_EFAULT        7
#define SERRNO_EEXIST        8
#define SERRNO_EINACTIVE     9
#define SERRNO_ENOSEND       10
#define SERRNO_ENORECV       11
#define SERRNO_ENOREPLY      12
#define SERRNO_EINTR         13
#define SERRNO_ETIMEOUT      14
#define SERRNO_EAGAIN        15

#endif /* !UAPI_lIBSEMINIX_ERRNO_TYPES_H */
