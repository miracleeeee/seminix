/*
 * Copyright 2020, Data61, CSIRO (ABN 41 687 119 230)
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */
#ifndef UAPI_LIBSEMINIX_TYPES_H
#define UAPI_LIBSEMINIX_TYPES_H

#include <utils/types.h>
#include "cap_types.h"
#include "deadline_types.h"
#include "device_types.h"
#include "errno_types.h"
#include "faults_types.h"
#include "ipc_types.h"
#include "mmap_types.h"
#include "objects_types.h"
#include "rights_types.h"
#include "rlimit_types.h"
#include "sched_types.h"
#include "signal_types.h"
#include "syscalls_types.h"
#include "sysctl_types.h"
#include "tcb_types.h"

/* cap desc define here. */
#define CAPDESC_CNODE_BITS  16

static inline int seminix_capdesc_push(int cd)
{
    return cd & ~BIT_GENMASK(CAPDESC_CNODE_BITS);
}

#define THIS_CNODE_CAPDESC  (INT_MAX - 2)
#define NO_COPYTCB          (INT_MAX - 2)

#endif /* !UAPI_LIBSEMINIX_TYPES_H */
