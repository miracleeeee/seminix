#ifndef UAPI_LIBSEMINIX_SYSCALLS_TYPES_H
#define UAPI_LIBSEMINIX_SYSCALLS_TYPES_H

/* System Calls */
#define __NR_seminix_CreateRlimit      (-1)
#define __NR_seminix_SetRlimit         (-2)
#define __NR_seminix_GetRlimit         (-3)
#define __NR_seminix_CNodeExpand       (-4)
#define __NR_seminix_CNodeDup          (-5)
#define __NR_seminix_CNodeMove         (-6)
#define __NR_seminix_CNodeRevoke       (-7)
#define __NR_seminix_CNodeDelete       (-8)
#define __NR_seminix_TCBWakeUpNew      (-9)
#define __NR_seminix_TCBConfigure      (-10)
#define __NR_seminix_TCBPriority       (-11)
#define __NR_seminix_TCBControl        (-12)
#define __NR_seminix_Yield             (-13)
#define __NR_seminix_DeviceControl     (-14)
#define __NR_seminix_Device            (-15)
#define __NR_seminix_DeviceIRQ         (-16)
#define __NR_seminix_SchedDeadline     (-17)
#define __NR_seminix_VMMAP             (-18)
#define __NR_seminix_VMProtModify      (-19)
#define __NR_seminix_VMUNMAP           (-20)
#define __NR_seminix_VMDUP             (-21)
#define __NR_seminix_VMAGET            (-22)
#define __NR_seminix_VMCOPY            (-23)
#define __NR_seminix_SetBadge          (-24)
#define __NR_seminix_SendIPC           (-25)
#define __NR_seminix_RecvIPC           (-26)
#define __NR_seminix_ReplyIPC          (-27)
#define __NR_seminix_SetSignal         (-28)
#define __NR_seminix_ClearSignal       (-29)
#define __NR_seminix_SendSignal        (-30)
#define __NR_seminix_SignalReturn      (-31)
#define __NR_seminix_SysCtl            (-32)
#define __NR_seminix_restart_syscall   (-33)

#define __NR_seminix_MAX               (33)

#define __NR_seminix_NoSysCall         (-34)

#define SYS_seminix_CreateRlimit      __NR_seminix_CreateRlimit
#define SYS_seminix_SetRlimit         __NR_seminix_SetRlimit
#define SYS_seminix_GetRlimit         __NR_seminix_GetRlimit

#define SYS_seminix_CNodeExpand       __NR_seminix_CNodeExpand
#define SYS_seminix_CNodeDup          __NR_seminix_CNodeDup
#define SYS_seminix_CNodeMove         __NR_seminix_CNodeMove
#define SYS_seminix_CNodeRevoke       __NR_seminix_CNodeRevoke
#define SYS_seminix_CNodeDelete       __NR_seminix_CNodeDelete

#define SYS_seminix_TCBWakeUpNew      __NR_seminix_TCBWakeUpNew
#define SYS_seminix_TCBConfigure      __NR_seminix_TCBConfigure
#define SYS_seminix_TCBPriority       __NR_seminix_TCBPriority
#define SYS_seminix_TCBControl        __NR_seminix_TCBControl
#define SYS_seminix_Yield             __NR_seminix_Yield

#define SYS_seminix_DeviceControl     __NR_seminix_DeviceControl
#define SYS_seminix_Device            __NR_seminix_Device
#define SYS_seminix_DeviceIRQ         __NR_seminix_DeviceIRQ

#define SYS_seminix_SchedDeadline     __NR_seminix_SchedDeadline

#define SYS_seminix_VMMAP             __NR_seminix_VMMAP
#define SYS_seminix_VMProtModify      __NR_seminix_VMProtModify
#define SYS_seminix_VMUNMAP           __NR_seminix_VMUNMAP
#define SYS_seminix_VMDUP             __NR_seminix_VMDUP
#define SYS_seminix_VMAGET            __NR_seminix_VMAGET
#define SYS_seminix_VMCOPY            __NR_seminix_VMCOPY

#define SYS_seminix_SetBadge          __NR_seminix_SetBadge
#define SYS_seminix_SendIPC           __NR_seminix_SendIPC
#define SYS_seminix_RecvIPC           __NR_seminix_RecvIPC
#define SYS_seminix_ReplyIPC          __NR_seminix_ReplyIPC

#define SYS_seminix_SetSignal         __NR_seminix_SetSignal
#define SYS_seminix_ClearSignal       __NR_seminix_ClearSignal
#define SYS_seminix_SendSignal        __NR_seminix_SendSignal
#define SYS_seminix_SignalReturn      __NR_seminix_SignalReturn

#define SYS_seminix_SysCtl            __NR_seminix_SysCtl

#define SYS_seminix_restart_syscall   __NR_seminix_restart_syscall

#define SYS_seminix_NoSysCall         __NR_seminix_NoSysCall

#endif /* !UAPI_LIBSEMINIX_SYSCALLS_TYPES_H */
