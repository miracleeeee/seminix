#ifndef UAPI_LIBSEMINIX_SYSCTL_TYPES_H
#define UAPI_LIBSEMINIX_SYSCTL_TYPES_H

#include <utils/cpumask.h>

#define SYSCTL_CMD_GET      1
#define SYSCTL_CMD_SET      2

#define SYSCTL_TYPE_ONLINECPUMASK     1
#define SYSCTL_TYPE_CPU_NR_TASK       2
#define SYSCTL_TYPE_TCB_WEIGHT        3
#define SYSCTL_TYPE_CPU_WEIGHT        4
#define SYSCTL_TYPE_IPCBUFFER         5
#define SYSCTL_TYPE_PAGESIZE          6
#define SYSCTL_TYPE_NR_PAGES_MIN      7
#define SYSCTL_TYPE_MMAPCOUNT_MAX     8

typedef struct sysctl {
    int type;
    union {
        cpumask_t cpumask;
        struct {
            int cpu;
            int nr_task;
        } cpu_task;
        struct {
            int tcb;
            unsigned long weight;
        } tcb_wi;
        struct {
            int cpu;
            unsigned long weight;
        } cpu_wi;
        struct {
            unsigned long uaddr;
        } ipcbuf;
        unsigned long pagesize;
        unsigned long nr_pages_min;
        unsigned long mmapcount_max;
    } sysctl;
} sysctl_t;

#endif /* !UAPI_LIBSEMINIX_SYSCTL_TYPES_H */
