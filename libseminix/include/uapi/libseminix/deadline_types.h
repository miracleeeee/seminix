#ifndef UAPI_lIBSEMINIX_DEADLINE_TYPES_H
#define UAPI_lIBSEMINIX_DEADLINE_TYPES_H

#define SCHED_DEADLINE_BIND     1
#define SCHED_DEADLINE_UNBIND   2

struct deadline_sched_param {
    unsigned long sched_runtime;
    unsigned long sched_deadline;
    unsigned long sched_period;
};

#endif /* !UAPI_lIBSEMINIX_DEADLINE_TYPES_H */
