#ifndef UAPI_LIBSEMINIX_RLIMIT_TYPES_H
#define UAPI_LIBSEMINIX_RLIMIT_TYPES_H

#define CAP_RLIMIT_INFINITY   (~0UL)

typedef struct caprlimit {
	unsigned long rlim_cur;
	unsigned long rlim_max;
} caprlimit_t;

#endif /* !UAPI_LIBSEMINIX_RLIMIT_TYPES_H */
