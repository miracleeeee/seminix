#ifndef UAPI_LIBSEMINIX_SIGNAL_TYPES_H
#define UAPI_LIBSEMINIX_SIGNAL_TYPES_H

#define SEMINIX_SIGNAL_USER0    0
#define SEMINIX_SIGNAL_USER1    1
#define SEMINIX_SIGNAL_KILL     2
#define SEMINIX_SIGNAL_MAX      3

typedef struct seminix_sigaction {
    void (*__sa_handler)(int);
    void (*__sa_restorer)(void);
} seminix_sigaction_t;

#endif /* !UAPI_LIBSEMINIX_SIGNAL_TYPES_H */
