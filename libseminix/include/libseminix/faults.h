/*
 * Copyright 2020, Data61, CSIRO (ABN 41 687 119 230)
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */
#ifndef LIBSEMINIX_FAULTS_H
#define LIBSEMINIX_FAULTS_H

#include <uapi/libseminix/types.h>
#include <asm/libseminix/faults.h>

#endif /* !LIBSEMINIX_FAULTS_H */
