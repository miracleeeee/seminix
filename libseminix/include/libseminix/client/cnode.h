#ifndef LIBSEMINIX_CLIENT_CNODE_H
#define LIBSEMINIX_CLIENT_CNODE_H

#include <uapi/libseminix/types.h>

int seminix_cnode_expand(int cnode);
int __seminix_cnode_dup(int cnode, int cd, int rights);
static inline int seminix_cnode_dup(int cd, int rights)
{
    return __seminix_cnode_dup(THIS_CNODE_CAPDESC, cd, rights);
}
int seminix_cnode_move(int cnode, int cd);
int seminix_cnode_revoke(int parent, int cd);
int seminix_cnode_delete(int parent, int cd);

#endif /* !LIBSEMINIX_CLIENT_CNODE_H */
