#ifndef LIBSEMINIX_CLIENT_RLIMIT_H
#define LIBSEMINIX_CLIENT_RLIMIT_H

#include <uapi/libseminix/types.h>

int __seminix_createrlimit(int cnode, int rights, seminix_object_t *object);

static inline int seminix_createrlimit(int rights, seminix_object_t *object)
{
    return __seminix_createrlimit(THIS_CNODE_CAPDESC, rights, object);
}

int seminix_uprlimit(int rlimit, int cap_type, unsigned long limit);
int seminix_downrlimit(int rlimit, int cap_type, unsigned long limit);
int seminix_getrlimit(int rlimit, int cap_type, struct caprlimit *crlim);

#endif /* !LIBSEMINIX_CLIENT_RLIMIT_H */
