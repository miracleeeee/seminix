#ifndef LIBSEMINIX_CLIENT_IPC_H
#define LIBSEMINIX_CLIENT_IPC_H

#include <time.h>
#include <uapi/libseminix/types.h>

int seminix_set_badge(int ep, int parent, unsigned long badge);

int seminix_send(int ep, seminix_message_t mess);
int seminix_send_timeout(int ep, seminix_message_t mess, struct timespec *ts);
int seminix_nbsend(int ep, seminix_message_t mess);
int seminix_send_reply(int ep, seminix_message_t send_mess, seminix_message_t *recv_mess);
int seminix_send_reply_timeout(int ep, seminix_message_t send_mess,
    seminix_message_t *recv_mess,  struct timespec *ts);
int seminix_nbsend_reply(int ep, seminix_message_t send_mess, seminix_message_t *recv_mess);
int seminix_nbsend_reply_timeout(int ep, seminix_message_t send_mess,
    seminix_message_t *recv_mess, struct timespec *ts);
int seminix_notify(int ep, seminix_message_t mess);

int seminix_recv(int ep, bool *reply, unsigned long *badge, seminix_message_t *mess);
int seminix_recv_timeout(int ep, bool *reply, unsigned long *badge, seminix_message_t *mess, struct timespec *ts);
int seminix_nbrecv(int ep, bool *reply, unsigned long *badge, seminix_message_t *mess);

int seminix_reply(int ep, unsigned long badge, seminix_message_t mess);

#endif /* !LIBSEMINIX_CLIENT_IPC_H */
