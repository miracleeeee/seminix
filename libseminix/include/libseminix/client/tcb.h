#ifndef LIBSEMINIX_CLIENT_TCB_H
#define LIBSEMINIX_CLIENT_TCB_H

#include <uapi/libseminix/types.h>

int seminix_tcb_wake_up_new(int new_tcb, int copy_tcb, unsigned long pc, unsigned long reg0, unsigned long stack, unsigned long flags,
    unsigned long *parent_tidptr, unsigned long *child_tidptr);
static inline int seminix_tcb_exec_new(int new_tcb, unsigned long pc, unsigned long reg0, unsigned long stack, unsigned long flags,
    unsigned long *parent_tidptr, unsigned long *child_tidptr)
{
    return seminix_tcb_wake_up_new(new_tcb, NO_COPYTCB, pc, reg0, stack, flags, parent_tidptr, child_tidptr);
}
int seminix_tcb_configure(int tcb, int maks, tcb_config_t *tcbconfig);
int seminix_tcb_setpriority(int tcb, int auth_tcb, int priority);
int seminix_tcb_setmcpriority(int tcb, int auth_tcb, int mcpriority);
int seminix_tcb_set_sched_params(int tcb, int auth_tcb, tcb_sched_params_t *params);
int seminix_tcb_get_sched_params(int tcb, tcb_sched_params_t *params);
int seminix_tcb_suspend(int tcb);
int seminix_tcb_resume(int tcb);
int seminix_tcb_status(int tcb);
int seminix_tcb_setaffinity(int tcb, int cpu);
int seminix_tcb_getaffinity(int tcb);
int seminix_tcb_kill(int tcb);
int seminix_tcb_tid(int tcb);

void seminix_yield(void);

#endif /* !LIBSEMINIX_CLIENT_TCB_H */
