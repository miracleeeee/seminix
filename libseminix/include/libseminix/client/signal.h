#ifndef LIBSEMINIX_CLIENT_SIGNAL_H
#define LIBSEMINIX_CLIENT_SIGNAL_H

#include <uapi/libseminix/types.h>

int seminix_signal(int sigctl, int tcb, int signal);
int seminix_sigaction(int sigmask, void (*sa_handler)(int));
int seminix_clear_sigaction(int sigmask);

#endif /* !LIBSEMINIX_CLIENT_SIGNAL_H */
