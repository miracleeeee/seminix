#ifndef LIBSEMINIX_CLIENT_DEVICE_H
#define LIBSEMINIX_CLIENT_DEVICE_H

#include <uapi/libseminix/types.h>

int __seminix_get_device(int devctl, int cnode, const char *compatible, int len);

static inline int seminix_get_device(int devctl, const char *compatible, int len)
{
    return __seminix_get_device(devctl, THIS_CNODE_CAPDESC, compatible, len);
}

int __seminix_device_getirq(int dev, int cnode, int index);

static inline int seminix_device_getirq(int dev, int index)
{
    return __seminix_device_getirq(dev, THIS_CNODE_CAPDESC, index);
}

int seminix_device_ackirq(int irq);
int seminix_device_setirq_handler(int irq, int ep);
int seminix_device_clearirq_handler(int irq);

#endif /* !LIBSEMINIX_CLIENT_DEVICE_H */
