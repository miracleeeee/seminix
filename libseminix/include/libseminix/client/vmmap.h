#ifndef LIBSEMINIX_CLIENT_VMMAP_H
#define LIBSEMINIX_CLIENT_VMMAP_H

#include <uapi/libseminix/types.h>

int seminix_vspace_mmap(int vspace,
    unsigned long addr, unsigned long len,
    unsigned long prot, unsigned long flags,
    int frame);
int seminix_vspace_pgprot_modify(int vspace,
    unsigned long addr, unsigned long len,
    unsigned long prot);
int seminix_vspace_munmap(int vspace,
    unsigned long addr, unsigned long len);
static inline int seminix_vspace_munmap_full(int vspace)
{
    return seminix_vspace_munmap(vspace, 0, 0);
}

int seminix_vspace_dup(int dst_vspace, int src_vspace,
    unsigned long addr, unsigned long len, int new_prot);
int seminix_vspace_getpage(int vspace, unsigned long addr);
int seminix_vspace_getvma(int vspace, unsigned long addr);
int seminix_vspace_copypage(int dst_vspace, int dst_page, int src_vspace, int src_page);
int seminix_vspace_copyvma(int dst_vspace, int dst_vma, int src_vspace, int src_vma);

#endif /* !LIBSEMINIX_CLIENT_VMMAP_H */
