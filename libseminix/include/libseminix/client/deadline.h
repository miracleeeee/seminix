#ifndef LIBSEMINIX_CLIENT_DEADLINE_H
#define LIBSEMINIX_CLIENT_DEADLINE_H

#include <uapi/libseminix/types.h>

int seminix_sched_deadline_bind(int deadline, int tcb);
int seminix_sched_deadline_unbind(int deadline);

#endif /* !LIBSEMINIX_CLIENT_DEADLINE_H */
