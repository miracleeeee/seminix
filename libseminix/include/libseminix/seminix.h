#ifndef LIBSEMINIX_SEMINIX_H
#define LIBSEMINIX_SEMINIX_H

#include <utils/processor.h>
#include <libseminix/syscall.h>
#include <libseminix/client.h>
#include <libseminix/faults.h>
#include <libseminix/functions.h>

#endif /* !LIBSEMINIX_SEMINIX_H */
