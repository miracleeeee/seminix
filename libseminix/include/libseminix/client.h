#ifndef LIBSEMINIX_CLIENT_H
#define LIBSEMINIX_CLIENT_H

#include "client/cnode.h"
#include "client/deadline.h"
#include "client/device.h"
#include "client/ipc.h"
#include "client/rlimit.h"
#include "client/signal.h"
#include "client/tcb.h"
#include "client/vmmap.h"
#include <asm/libseminix/client.h>

int seminix_online_cpumask(cpumask_t *cpumask);
int seminix_cpu_nr_task(int cpu);
int seminix_get_tcb_weight(int tcb, unsigned long *weight);
int seminix_get_cpu_weight(int cpu, unsigned long *weight);
unsigned long seminix_get_ipcbuffer(void);
unsigned long seminix_get_pagesize(void);
unsigned long seminix_get_nr_pages_min(void);
int seminix_set_nr_pages_min(int sysctl, unsigned long nr_pages);
unsigned long seminix_get_mmapcount_max(void);
int seminix_set_mmapcount_max(int sysctl, unsigned long mmapcount_max);

#endif /* !LIBSEMINIX_CLIENT_H */
