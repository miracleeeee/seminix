/*
 * Copyright 2020, Data61, CSIRO (ABN 41 687 119 230)
 * Copyright 2015, 2016 Hesham Almatary <heshamelmatary@gmail.com>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */
#ifndef LIBSEMINIX_FUNCTIONS_H
#define LIBSEMINIX_FUNCTIONS_H

#include <uapi/libseminix/types.h>

extern __thread seminix_ipc_buffer_t *__seminix_ipc_buffer;

static inline seminix_ipc_buffer_t *seminix_get_ipc_buffer(void)
{
    return __seminix_ipc_buffer;
}

static inline seminix_message_t seminix_get_message(void)
{
    return seminix_get_ipc_buffer()->message;
}

static inline uintptr_t seminix_get_mr(int i)
{
    return seminix_get_ipc_buffer()->msg[i];
}

static inline void seminix_set_mr(int i, uintptr_t mr)
{
    seminix_get_ipc_buffer()->msg[i] = mr;
}

static inline unsigned long seminix_get_badge(void)
{
    return seminix_get_ipc_buffer()->badge;
}

static inline bool seminix_get_reply(void)
{
    return seminix_get_ipc_buffer()->reply;
}

static inline int seminix_get_capdesc(int index)
{
    return seminix_get_ipc_buffer()->capdesc[index];
}

static inline void seminix_set_capdesc(int cd, int index)
{
    seminix_get_ipc_buffer()->capdesc[index] = cd;
}

static inline struct seminix_ipc_notify seminix_get_ipc_notify(int index)
{
    return seminix_get_ipc_buffer()->notify[index];
}

static inline seminix_message_t seminix_get_ipc_irqnotify(int index)
{
    return seminix_get_ipc_buffer()->irqnotify[index];
}

#endif /* !LIBSEMINIX_FUNCTIONS_H */
