#include <string.h>
#include <libseminix/client/device.h>
#include <libseminix/syscall.h>

int __seminix_get_device(int devctl, int cnode, const char *compatible, int len)
{
    int op = DEVCTL_OP_GET_COMPATIBLE;
    char comp[DEVICE_COMPATIBLE_MAX];

    if (len > DEVICE_COMPATIBLE_MAX)
        len = DEVICE_COMPATIBLE_MAX;
    strlcpy(comp, compatible, len);

    return __syscall(SYS_seminix_DeviceControl, devctl, op, cnode, comp, len);
}

int __seminix_device_getirq(int dev, int cnode, int index)
{
    int op = DEVICE_OP_GET_IRQ;

    return __syscall(SYS_seminix_Device, dev, op, cnode, index);
}

int seminix_device_ackirq(int irq)
{
    int op = DEVIRQ_OP_ACK;

    return __syscall(SYS_seminix_DeviceIRQ, irq, op);
}

int seminix_device_setirq_handler(int irq, int ep)
{
    int op = DEVIRQ_OP_SETIRQ;

    return __syscall(SYS_seminix_DeviceIRQ, irq, op, ep);
}

int seminix_device_clearirq_handler(int irq)
{
    int op = DEVIRQ_OP_CLEARIRQ;

    return __syscall(SYS_seminix_DeviceIRQ, irq, op);
}
