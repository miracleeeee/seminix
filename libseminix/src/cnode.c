#include <libseminix/client/cnode.h>
#include <libseminix/syscall.h>

int seminix_cnode_expand(int cnode)
{
    return __syscall(SYS_seminix_CNodeExpand, cnode);
}

int __seminix_cnode_dup(int cnode, int cd, int rights)
{
    return __syscall(SYS_seminix_CNodeDup, cnode, cd, rights);
}

int seminix_cnode_move(int cnode, int cd)
{
    return __syscall(SYS_seminix_CNodeMove, cnode, cd);
}

int seminix_cnode_revoke(int parent, int cd)
{
    return __syscall(SYS_seminix_CNodeRevoke, parent, cd);
}

int seminix_cnode_delete(int parent, int cd)
{
    return __syscall(SYS_seminix_CNodeDelete, parent, cd);
}
