#include <utils/linkage.h>
#include <libseminix/client/signal.h>
#include <libseminix/syscall.h>

extern asmlinkage void _sigreturn(void);

int seminix_signal(int sigctl, int tcb, int signal)
{
    return __syscall(SYS_seminix_SendSignal, sigctl, tcb, signal);
}

int seminix_sigaction(int sigmask, void (*sa_handler)(int))
{
    seminix_sigaction_t action;

    if (!sa_handler)
        return -SERRNO_EINVAL;

    action.__sa_handler = sa_handler;
    action.__sa_restorer = _sigreturn;
    sigmask &= BIT_GENMASK(SEMINIX_SIGNAL_MAX);

    return __syscall(SYS_seminix_SetSignal, sigmask, &action);
}

int seminix_clear_sigaction(int sigmask)
{
    return __syscall(SYS_seminix_ClearSignal, sigmask);
}
