#include <libseminix/client/rlimit.h>
#include <libseminix/syscall.h>

int __seminix_createrlimit(int cnode, int rights, seminix_object_t *object)
{
    seminix_object_t data;

    if (!object || seminix_object_type_invalid(object->type))
        return -SERRNO_EINVAL;

    memcpy(&data, object, sizeof (*object));

    return __syscall(SYS_seminix_CreateRlimit, cnode, rights, &data);
}

int seminix_uprlimit(int rlimit, int cap_type, unsigned long limit)
{
    if (seminix_cap_type_invalid(cap_type))
        return -SERRNO_EINVAL;

    return __syscall(SYS_seminix_SetRlimit, rlimit, cap_type, limit, true);
}

int seminix_downrlimit(int rlimit, int cap_type, unsigned long limit)
{
    if (seminix_cap_type_invalid(cap_type))
        return -SERRNO_EINVAL;

    return __syscall(SYS_seminix_SetRlimit, rlimit, cap_type, limit, false);
}

int seminix_getrlimit(int rlimit, int cap_type, struct caprlimit *crlim)
{
    int ret;
    struct caprlimit rlim;

    if (seminix_cap_type_invalid(cap_type) || !crlim)
        return -SERRNO_EINVAL;

    ret = __syscall(SYS_seminix_GetRlimit, rlimit, cap_type, &rlim);
    if (ret)
        return ret;

    crlim->rlim_cur = rlim.rlim_cur;
    crlim->rlim_max = rlim.rlim_max;

    return 0;
}
