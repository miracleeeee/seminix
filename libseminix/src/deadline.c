#include <libseminix/client/deadline.h>
#include <libseminix/syscall.h>

int seminix_sched_deadline_bind(int deadline, int tcb)
{
    return __syscall(SYS_seminix_SchedDeadline, deadline, SCHED_DEADLINE_BIND, tcb);
}

int seminix_sched_deadline_unbind(int deadline)
{
    return __syscall(SYS_seminix_SchedDeadline, deadline, SCHED_DEADLINE_UNBIND, 0);
}
