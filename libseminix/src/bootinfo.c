#include <libseminix/functions.h>
#include <libseminix/client.h>

/** Userland per-thread IPC buffer address **/
__thread seminix_ipc_buffer_t *__seminix_ipc_buffer;

static void __attribute__((constructor(101))) ipc_buffer_get(void)
{
    __seminix_ipc_buffer = (seminix_ipc_buffer_t *)seminix_get_ipcbuffer();
}
