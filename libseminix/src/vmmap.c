#include <libseminix/client/vmmap.h>
#include <libseminix/syscall.h>

int seminix_vspace_mmap(int vspace,
    unsigned long addr, unsigned long len,
    unsigned long prot, unsigned long flags,
    int frame)
{
    return __syscall(SYS_seminix_VMMAP, vspace, addr, len,
            prot, flags, frame);
}

int seminix_vspace_pgprot_modify(int vspace,
    unsigned long addr, unsigned long len,
    unsigned long prot)
{
    return __syscall(SYS_seminix_VMProtModify, vspace, addr, len, prot);
}

int seminix_vspace_munmap(int vspace, unsigned long addr, unsigned long len)
{
    return __syscall(SYS_seminix_VMUNMAP, vspace, addr, len);
}

int seminix_vspace_dup(int dst_vspace, int src_vspace,
    unsigned long addr, unsigned long len, int new_prot)
{
    return __syscall(SYS_seminix_VMDUP, dst_vspace, src_vspace, addr, len, new_prot);
}

int seminix_vspace_getpage(int vspace, unsigned long addr)
{
    return __syscall(SYS_seminix_VMAGET, vspace, addr, VSPACE_OP_PAGE);
}

int seminix_vspace_getvma(int vspace, unsigned long addr)
{
    return __syscall(SYS_seminix_VMAGET, vspace, addr, VSPACE_OP_AREA);
}

static inline int seminix_vspace_copy(int dst_vspace, int dst_index, int src_vspace, int src_index, int op)
{
    return __syscall(SYS_seminix_VMCOPY, dst_vspace, dst_index, src_vspace, src_index, op);
}

int seminix_vspace_copypage(int dst_vspace, int dst_page, int src_vspace, int src_page)
{
    return seminix_vspace_copy(dst_vspace, dst_page, src_vspace, src_page, VSPACE_OP_PAGE);
}

int seminix_vspace_copyvma(int dst_vspace, int dst_vma, int src_vspace, int src_vma)
{
    return seminix_vspace_copy(dst_vspace, dst_vma, src_vspace, src_vma, VSPACE_OP_AREA);
}
