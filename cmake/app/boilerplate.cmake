# SPDX-License-Identifier: Apache-2.0

include_guard(GLOBAL)

# CMake version 3.20 is the real minimum supported version.
#
# Under these restraints we use a second 'cmake_minimum_required'
# invocation in every toplevel CMakeLists.txt.
cmake_minimum_required(VERSION 3.20.0)

# CMP0002: "Logical target names must be globally unique"
cmake_policy(SET CMP0002 NEW)

# CMP0079: "target_link_libraries() allows use with targets in other directories"
cmake_policy(SET CMP0079 NEW)

if(${CMAKE_CURRENT_SOURCE_DIR} STREQUAL ${CMAKE_CURRENT_BINARY_DIR})
  message(FATAL_ERROR "Source directory equals build directory.\
    In-source builds are not supported.\
    Please specify a build directory, e.g. cmake -Bbuild -H.")
endif()

set(SEMINIX_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR} CACHE PATH "Kernel Source Directory")
set(SEMINIX_BINARY_DIR ${CMAKE_CURRENT_BINARY_DIR} CACHE PATH "Kernel Binary Directory")
message(STATUS "Kernel source: ${SEMINIX_SOURCE_DIR}")

set(AUTOCONF_H ${APPLICATION_BINARY_DIR}/include/generated/autoconf.h)
# Re-configure (Re-execute all CMakeLists.txt code) when autoconf.h changes
set_property(DIRECTORY APPEND PROPERTY CMAKE_CONFIGURE_DEPENDS ${AUTOCONF_H})

#
# Import more CMake functions and macros
#
include(CheckCCompilerFlag)
include(CheckCXXCompilerFlag)
include(${SEMINIX_KERNEL_DIR}/cmake/extensions.cmake)
include(${SEMINIX_KERNEL_DIR}/cmake/git.cmake)
include(${SEMINIX_KERNEL_DIR}/cmake/version.cmake)

seminix_check_cache(APPLICATION_SOURCE_DIR REQUIRED)
seminix_check_cache(APPLICATION_BINARY_DIR REQUIRED)
message(STATUS "Application: ${APPLICATION_SOURCE_DIR}")

include(${SEMINIX_KERNEL_DIR}/cmake/python.cmake)
include(${SEMINIX_KERNEL_DIR}/cmake/ccache.cmake)

if(NOT DEFINED USER_CACHE_DIR)
	find_appropriate_cache_directory(USER_CACHE_DIR)
  set(USER_CACHE_DIR ${USER_CACHE_DIR} CACHE PATH "")
endif()
message(STATUS "Cache files will be written to: ${USER_CACHE_DIR}")

# Check that ARCH has been provided.
set(ENV_ARCH $ENV{ARCH})
if ((NOT DEFINED ARCH) AND (DEFINED ENV_ARCH))
  set(ARCH ${ENV_ARCH} CACHE STRING "")
endif()

if (NOT DEFINED ARCH)
  message(FATAL_ERROR "\nARCH is not defined, it is a required platform variable.
Example:
  -DARCH=arm64 or export ARCH=arm64
")
endif()
message(STATUS "ARCH: ${ARCH}")

# Equivalent to rm -rf build/*
add_custom_target(
  pristine
  COMMAND ${CMAKE_COMMAND} -DBINARY_DIR=${APPLICATION_BINARY_DIR}
    -DSOURCE_DIR=${APPLICATION_SOURCE_DIR}
    -P ${SEMINIX_KERNEL_DIR}/cmake/pristine.cmake
)

# Prevent CMake from testing the toolchain
set(CMAKE_C_COMPILER_FORCED   1)
set(CMAKE_CXX_COMPILER_FORCED 1)

include(${SEMINIX_KERNEL_DIR}/cmake/verify-toolchain.cmake)
include(${SEMINIX_KERNEL_DIR}/cmake/host-tools.cmake)
include(${SEMINIX_KERNEL_DIR}/cmake/target_toolchain.cmake)

enable_language(C CXX ASM)

include(${SEMINIX_KERNEL_DIR}/cmake/kconfig.cmake)

include(${SEMINIX_KERNEL_DIR}/cmake/dts.cmake)

if(CONFIG_BUILD_VERSION_AUTO)
	set(SEMINIX_VERSION_STRING "\"${SEMINIX_VERSION_PREFIX}${CONFIG_LOCALVERSION}-${BUILD_VERSION}\"")
else()
	set(SEMINIX_VERSION_STRING "\"${SEMINIX_VERSION_PREFIX}${CONFIG_LOCALVERSION}\"")
endif()
configure_file(${SEMINIX_SOURCE_DIR}/version.h.in ${APPLICATION_BINARY_DIR}/include/generated/version.h)
# Re-configure (Re-execute all CMakeLists.txt code) when autoconf.h changes
set_property(DIRECTORY APPEND PROPERTY CMAKE_CONFIGURE_DEPENDS ${SEMINIX_SOURCE_DIR}/VERSION)

include(${SEMINIX_KERNEL_DIR}/cmake/app/common.cmake)
include(${SEMINIX_KERNEL_DIR}/cmake/app/kernel.cmake)

get_property(TOPT GLOBAL PROPERTY TOPT)
set_ifndef(  TOPT -Wl,-T) # clang doesn't pick -T for some reason and complains,
                          # while -Wl,-T works for both, gcc and clang

seminix_check_compiler_flag(C "" toolchain_is_ok)
assert(toolchain_is_ok "The toolchain is unable to build a dummy C file. See CMakeError.log.")
seminix_check_compiler_flag(ASM "" toolchain_is_ok)
assert(toolchain_is_ok "The toolchain is unable to build a dummy ASM file. See CMakeError.log.")

set(CMAKE_EXECUTABLE_SUFFIX .elf)

set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS "Debug;Release;RelWithDebInfo;MinSizeRel")
mark_as_advanced(CLEAR CMAKE_BUILD_TYPE)

set(ARCHIVE_APPDATA_FILE "${APPLICATION_BINARY_DIR}/rootserver/archive_appdata.S" CACHE PATH "")
set(ROOTSERVER_DIR ${APPLICATION_BINARY_DIR}/rootserver CACHE PATH "")
file(MAKE_DIRECTORY ${ROOTSERVER_DIR})

set(target_platform ${CONFIG_TARGET_PLATFORM} CACHE PATH "")
