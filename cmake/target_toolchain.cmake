# SPDX-License-Identifier: Apache-2.0

# Don't inherit compiler flags from the environment
foreach(var AFLAGS CFLAGS CXXFLAGS CPPFLAGS LDFLAGS)
  if(DEFINED ENV{${var}})
    message(WARNING "The environment variable '${var}' was set to $ENV{${var}},
but seminix ignores flags from the environment. Use 'cmake -DEXTRA_${var}=$ENV{${var}}' instead.")
    unset(ENV{${var}})
  endif()
endforeach()

# Configure the toolchain based on what SDK/toolchain is in use.
include(${SEMINIX_KERNEL_DIR}/cmake/toolchain/${SEMINIX_TOOLCHAIN}/generic.cmake)

unset(CMAKE_C_COMPILER)
unset(CMAKE_C_COMPILER CACHE)

unset(CMAKE_CXX_COMPILER)
unset(CMAKE_CXX_COMPILER CACHE)

unset(CMAKE_LINKER)
unset(CMAKE_LINKER CACHE)

# No official documentation exists for the "Generic" value, except their wiki.
#
# https://gitlab.kitware.com/cmake/community/wikis/doc/cmake/CrossCompiling:
#   CMAKE_SYSTEM_NAME : this one is mandatory, it is the name of the target
#   system, i.e. the same as CMAKE_SYSTEM_NAME would have if CMake would run
#   on the target system.  Typical examples are "Linux" and "Windows". This
#   variable is used for constructing the file names of the platform files
#   like Linux.cmake or Windows-gcc.cmake. If your target is an embedded
#   system without OS set CMAKE_SYSTEM_NAME to "Generic".
set(CMAKE_SYSTEM_NAME Generic)

# https://cmake.org/cmake/help/latest/variable/CMAKE_SYSTEM_PROCESSOR.html:
#   The name of the CPU CMake is building for.
#
# https://gitlab.kitware.com/cmake/community/wikis/doc/cmake/CrossCompiling:
#   CMAKE_SYSTEM_PROCESSOR : optional, processor (or hardware) of the
#   target system. This variable is not used very much except for one
#   purpose, it is used to load a
#   CMAKE_SYSTEM_NAME-compiler-CMAKE_SYSTEM_PROCESSOR.cmake file,
#   which can be used to modify settings like compiler flags etc. for
#   the target
set(CMAKE_SYSTEM_PROCESSOR ${ARCH})

# https://cmake.org/cmake/help/latest/variable/CMAKE_SYSTEM_VERSION.html:
#   When the CMAKE_SYSTEM_NAME variable is set explicitly to enable cross
#   compiling then the value of CMAKE_SYSTEM_VERSION must also be set
#   explicitly to specify the target system version.
set(CMAKE_SYSTEM_VERSION ${SEMINIX_VERSION})

# A toolchain consist of a compiler and a linker.
# In seminix, toolchains require a port under cmake/toolchain/.
# Each toolchain port must set COMPILER and LINKER.
# E.g. toolchain/llvm may pick {clang, ld} or {clang, lld}.
add_custom_target(bintools)

include(${SEMINIX_KERNEL_DIR}/cmake/toolchain/${SEMINIX_TOOLCHAIN}/${COMPILER}/target.cmake)
include(${SEMINIX_KERNEL_DIR}/cmake/toolchain/${SEMINIX_TOOLCHAIN}/${LINKER}/target.cmake)
include(${SEMINIX_KERNEL_DIR}/cmake/toolchain/bintools/bintools_template.cmake)
include(${SEMINIX_KERNEL_DIR}/cmake/toolchain/bintools/${BINTOOLS}/target.cmake)

file(MD5 ${CMAKE_C_COMPILER} CMAKE_C_COMPILER_MD5_SUM)
set(TOOLCHAIN_SIGNATURE ${CMAKE_C_COMPILER_MD5_SUM})

# Extend the CMAKE_C_COMPILER_MD5_SUM with the compiler signature.
string(MD5 COMPILER_SIGNATURE ${CMAKE_C_COMPILER}_${CMAKE_C_COMPILER_ID}_${CMAKE_C_COMPILER_VERSION})
set(TOOLCHAIN_SIGNATURE ${TOOLCHAIN_SIGNATURE}_${COMPILER_SIGNATURE} CACHE STRING "")
