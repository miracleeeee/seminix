# SPDX-License-Identifier: Apache-2.0
include_guard(GLOBAL)

include(${SEMINIX_KERNEL_DIR}/cmake/helpers/cpio.cmake)

function(make_appdata_archive output_name)
  MakeCPIO(commands ${output_name} ${ARGN})
  add_custom_command(
    OUTPUT ${output_name}
    COMMAND rm -f ${output_name}
    COMMAND rm -f ${output_name}.cpio
    COMMAND ${commands}
    COMMAND ${SHELL} -c
    "echo 'X.section .archive_appdata,\"a\"X.globl __archive_appdata_start, \
    __archive_appdata_endX__archive_appdata_start:X.incbin \"${output_name}.cpio\"X__archive_appdata_end:X' | tr X '\\n'"
    > ${output_name}
    BYPRODUCTS
    ${output_name}.cpio
    DEPENDS
    ${ARGN}
    VERBATIM
  )
  add_custom_target(appdata_archive DEPENDS ${output_name})
endfunction()
