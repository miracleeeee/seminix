# SPDX-License-Identifier: Apache-2.0

# Configures CMake for using GCC

find_program(CMAKE_C_COMPILER gcc REQUIRED)
find_program(CMAKE_CXX_COMPILER g++ REQUIRED)

set(NOSTDINC "")

execute_process(
  COMMAND ${CMAKE_C_COMPILER} --print-file-name=include
  OUTPUT_VARIABLE _OUTPUT
)

string(REGEX REPLACE "\n" "" _OUTPUT "${_OUTPUT}")

list(APPEND NOSTDINC ${_OUTPUT})
