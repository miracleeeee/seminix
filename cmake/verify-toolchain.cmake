# The purpose of this file is to verify that required variables has been
# defined for proper toolchain use.

if(NOT DEFINED SEMINIX_TOOLCHAIN)
	set(SEMINIX_TOOLCHAIN $ENV{SEMINIX_TOOLCHAIN})
endif()

if(NOT SEMINIX_TOOLCHAIN AND
    (CROSS_COMPILE OR (DEFINED ENV{CROSS_COMPILE})))
  set(SEMINIX_TOOLCHAIN cross-compile)
endif()

if(NOT DEFINED SEMINIX_TOOLCHAIN)
  message(FATAL_ERROR "No toolchain defining the available tools, The sample:
    SEMINIX_TOOLCHAIN=llvm/host
")
endif()
