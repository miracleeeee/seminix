# SPDX-License-Identifier: Apache-2.0

#.rst:
# git.cmake
# ---------
# If the user didn't already define BUILD_VERSION then try to initialize
# it with the output of "git describe". Warn but don't error if
# everything fails and leave BUILD_VERSION undefined.

# https://cmake.org/cmake/help/latest/module/FindGit.html
find_package(Git REQUIRED)
if(GIT_FOUND)
  execute_process(
    COMMAND ${GIT_EXECUTABLE} describe --abbrev=12 --always
    WORKING_DIRECTORY                ${SEMINIX_SOURCE_DIR}
    OUTPUT_VARIABLE                  BUILD_VERSION
    OUTPUT_STRIP_TRAILING_WHITESPACE
    ERROR_STRIP_TRAILING_WHITESPACE
    ERROR_VARIABLE                   stderr
    RESULT_VARIABLE                  return_code
  )
  if(return_code)
    message(FATAL_ERROR "git describe failed: ${stderr}")
  elseif(NOT "${stderr}" STREQUAL "")
    message(FATAL_ERROR "git describe warned: ${stderr}")
  endif()
endif()
