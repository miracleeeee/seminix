#include <seminix/syscall.h>

SYSCALL_DEFINE6(mmap, unsigned long, addr, unsigned long, len,
		unsigned long, prot, unsigned long, flags,
		unsigned long, fd, off_t, off)
{
    printk("mmap addr 0x%lx len 0x%lx prot 0x%lx flags 0x%lx fd %ld off %ld\n",
        addr, len, prot, flags, fd, off);
    return -ENOMEM;
}
