#include <seminix/syscall.h>
#include <seminix/time.h>

SYSCALL_DEFINE6(futex, u32 __user *, uaddr, int, op, u32, val,
		struct __kernel_timespec __user *, utime, u32 __user *, uaddr2,
		u32, val3)
{
    return 0;
}
