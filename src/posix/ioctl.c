#include <seminix/syscall.h>
#include <seminix/uaccess.h>

#include <bits/ioctl.h>
#define __NEED_struct_winsize
#include <bits/alltypes.h>

static struct winsize test_winsize = {
    .ws_col = 80,
    .ws_row = 80,
    .ws_xpixel = 80,
    .ws_ypixel = 80,
};

SYSCALL_DEFINE3(ioctl, unsigned int, fd, unsigned int, cmd, unsigned long, arg)
{
    // TODO
    switch (cmd) {
    case TIOCGWINSZ:
        if (copy_to_user((void *)arg, &test_winsize, sizeof (struct winsize)))
            return -EFAULT;
        break;
    default:
        panic("ioctl: fd %d cmd %x\n", fd, cmd);
    }
    return 0;
}
