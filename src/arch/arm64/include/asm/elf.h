#ifndef ASM_ELF_H
#define ASM_ELF_H

#include <utils/elf.h>
#include <asm/processor.h>

/* update AT_VECTOR_SIZE_ARCH if the number of NEW_AUX_ENT entries changes */
#undef ARCH_DLINFO
#define ARCH_DLINFO							\
do {									\
    /*								\
     * Should always be nonzero unless there's a kernel bug.	\
     * If we haven't determined a sensible value to give to		\
     * userspace, omit the entry:					\
     */								\
    if (likely(signal_minsigstksz))					\
        NEW_AUX_ENT(AT_MINSIGSTKSZ, signal_minsigstksz);	\
    else								\
        NEW_AUX_ENT(AT_IGNORE, 0);				\
} while (0)

#endif /* !ASM_ELF_H */
