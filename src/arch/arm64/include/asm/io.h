#ifndef __ASM_IO_H_
#define __ASM_IO_H_

#include <utils/io.h>
#include <asm/pgtable.h>

/*
 * I/O memory mapping functions.
 */
extern void __iomem *__ioremap(phys_addr_t phys_addr, size_t size, pgprot_t prot);
#define ioremap(addr, size)		__ioremap((addr), (size), __pgprot(PROT_DEVICE_nGnRE))

#endif /* !__ASM_IO_H_ */
