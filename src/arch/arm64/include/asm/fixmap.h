/*
 * fixmap.h: compile-time virtual memory allocation
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file "COPYING" in the main directory of this archive
 * for more details.
 *
 * Copyright (C) 1998 Ingo Molnar
 * Copyright (C) 2013 Mark Salter <msalter@redhat.com>
 *
 * Adapted from arch/x86 version.
 *
 */

#ifndef ASM_ARM64_FIXMAP_H
#define ASM_ARM64_FIXMAP_H

#ifndef __ASSEMBLY__

#include <utils/sizes.h>
#include <asm/boot.h>
#include <asm/pgtable-prot.h>

/*
 * Here we define all the compile-time 'special' virtual
 * addresses. The point is to have a constant address at
 * compile time, but to set the physical address only
 * in the boot process.
 *
 * Each enum increment in these 'compile-time allocated'
 * memory buffers is page-sized. Use set_fixmap(idx,phys)
 * to associate physical memory with a fixmap index.
 */
enum fixed_addresses {
    FIX_HOLE,

#define FIX_IOREMAP_SIZE        (SZ_64M + SZ_2M)
    FIX_IOREMAP_END,
    FIX_IOREMAP = FIX_IOREMAP_END + FIX_IOREMAP_SIZE / UTILS_PAGE_SIZE - 1,

    /*
     * Reserve a virtual window for the FDT that is 2 MB larger than the
     * maximum supported size, and put it at the top of the fixmap region.
     * The additional space ensures that any FDT that does not exceed
     * MAX_FDT_SIZE can be mapped regardless of whether it crosses any
     * 2 MB alignment boundaries.
     *
     * Keep this at the top so it remains 2 MB aligned.
     */
#define FIX_FDT_SIZE		(MAX_FDT_SIZE + SZ_2M)
    FIX_FDT_END,
    FIX_FDT = FIX_FDT_END + FIX_FDT_SIZE / UTILS_PAGE_SIZE - 1,

    FIX_CONSOLE_MEM_BASE,
    FIX_SPINTABLE_BASE,
    __end_of_permanent_fixed_addresses,

    /*
     * Used for kernel page table creation, so unmapped memory may be used
     * for tables.
     */
    FIX_PTE,
    FIX_PMD,
    FIX_PUD,
    FIX_PGD,

    __end_of_fixed_addresses
};

#define FIXADDR_SIZE	(__end_of_permanent_fixed_addresses << UTILS_PAGE_SHIFT)
#define FIXADDR_START	(FIXADDR_TOP - FIXADDR_SIZE)

#define FIXMAP_PAGE_IO      __pgprot(PROT_DEVICE_nGnRE)

extern void *__fixmap_remap_phys(u64 raw_virt, phys_addr_t phys, size_t size,
    pgprot_t prot, phys_addr_t (*pgtable_alloc)(int));
extern void *__fixmap_remap_console(phys_addr_t con_phys, pgprot_t prot);
extern void *__fixmap_remap_spintable(phys_addr_t release_addr, pgprot_t prot);

extern void *fixmap_remap_fdt(phys_addr_t dt_phys);

extern void early_fixmap_init(void);

#define __early_set_fixmap __set_fixmap

#define __late_set_fixmap __set_fixmap
#define __late_clear_fixmap(idx) __set_fixmap((idx), 0, FIXMAP_PAGE_CLEAR)

extern void __set_fixmap(enum fixed_addresses idx, phys_addr_t phys, pgprot_t prot);

#include <asm-generic/fixmap.h>

#endif /* !__ASSEMBLY__ */
#endif /* !ASM_ARM64_FIXMAP_H */
