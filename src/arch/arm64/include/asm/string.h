#ifndef ASM_STRING_H
#define ASM_STRING_H

#include <string.h>

#define __ARCH_HAVE_MEMCHR

#define __ARCH_HAVE_MEMCMP

#define __ARCH_HAVE_MEMCPY
extern void *__memcpy(void *, const void *, size_t);

#define __ARCH_HAVE_MEMMOVE
extern void *__memmove(void *, const void *, size_t);

#define __ARCH_HAVE_MEMSET
extern void *__memset(void *, int, size_t);

#define __ARCH_HAVE_STRCHR

#define __ARCH_HAVE_STRRCHR

#define __ARCH_HAVE_STRCMP

#define __ARCH_HAVE_STRNCMP

#define __ARCH_HAVE_STRLEN

#define __ARCH_HAVE_STRNLEN

#endif /* !ASM_STRING_H */
