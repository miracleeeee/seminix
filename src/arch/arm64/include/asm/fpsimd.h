#ifndef ASM_FPSIMD_H
#define ASM_FPSIMD_H

#include <signal.h>
#include <errno.h>
#include <seminix/linkage.h>
#include <seminix/cache.h>
#include <asm/ptrace.h>
#include <asm/cpufeature.h>
#include <asm/processor.h>

struct tcb;

extern void fpsimd_save_state(struct user_fpsimd_state *state);
extern void fpsimd_load_state(struct user_fpsimd_state *state);

extern void fpsimd_save(void);

extern void fpsimd_thread_switch(struct tcb *next);
extern void fpsimd_flush_thread(void);

extern void fpsimd_signal_preserve_current_state(void);
extern void fpsimd_preserve_current_state(void);
extern void fpsimd_restore_current_state(void);
extern void fpsimd_update_current_state(struct user_fpsimd_state const *state);

extern void fpsimd_bind_task_to_cpu(void);
extern void fpsimd_bind_state_to_cpu(struct user_fpsimd_state *state);

extern void fpsimd_flush_task_state(struct tcb *target);
extern void fpsimd_flush_cpu_state(void);
extern void sve_flush_cpu_state(void);

/* Maximum VL that SVE VL-agnostic software can transparently support */
#define SVE_VL_ARCH_MAX 0x100

/* Offset of FFR in the SVE register dump */
static inline size_t sve_ffr_offset(int vl)
{
    return SVE_SIG_FFR_OFFSET(sve_vq_from_vl(vl)) - SVE_SIG_REGS_OFFSET;
}

static inline void *sve_pffr(struct thread_struct *thread)
{
    return (char *)thread->sve_state + sve_ffr_offset(thread->sve_vl);
}

extern void sve_save_state(void *state, u32 *pfpsr);
extern void sve_load_state(void const *state, u32 const *pfpsr,
               unsigned long vq_minus_1);
extern unsigned int sve_get_vl(void);

struct arm64_cpu_capabilities;
extern void sve_kernel_enable(const struct arm64_cpu_capabilities *__unused);

extern u64 read_zcr_features(void);

extern int __ro_after_init sve_max_vl;

#ifdef CONFIG_ARM64_SVE

extern size_t sve_state_size(struct tcb const *task);

extern void sve_alloc(struct tcb *task);
extern void fpsimd_release_task(struct tcb *task);
extern void fpsimd_sync_to_sve(struct tcb *task);
extern void sve_sync_to_fpsimd(struct tcb *task);
extern void sve_sync_from_fpsimd_zeropad(struct tcb *task);

extern int sve_set_vector_length(struct tcb *task,
                 unsigned long vl, unsigned long flags);

extern int sve_set_current_vl(unsigned long arg);
extern int sve_get_current_vl(void);

static inline void sve_user_disable(void)
{
    sysreg_clear_set(cpacr_el1, CPACR_EL1_ZEN_EL0EN, 0);
}

static inline void sve_user_enable(void)
{
    sysreg_clear_set(cpacr_el1, 0, CPACR_EL1_ZEN_EL0EN);
}

/*
 * Probing and setup functions.
 * Calls to these functions must be serialised with one another.
 */
extern void __init sve_init_vq_map(void);
extern void sve_update_vq_map(void);
extern int sve_verify_vq_map(void);
extern void __init sve_setup(void);

#else /* ! CONFIG_ARM64_SVE */

static inline void sve_alloc(struct tcb *task) { }
static inline void fpsimd_release_task(struct tcb *task) { }
static inline void sve_sync_to_fpsimd(struct tcb *task) { }
static inline void sve_sync_from_fpsimd_zeropad(struct tcb *task) { }

static inline int sve_set_current_vl(unsigned long arg)
{
    return -EINVAL;
}

static inline int sve_get_current_vl(void)
{
    return -EINVAL;
}

static inline void sve_user_disable(void) { BUILD_BUG(); }
static inline void sve_user_enable(void) { BUILD_BUG(); }

static inline void sve_init_vq_map(void) { }
static inline void sve_update_vq_map(void) { }
static inline int sve_verify_vq_map(void) { return 0; }
static inline void sve_setup(void) { }

#endif /* ! CONFIG_ARM64_SVE */

asmlinkage void do_sve_acc(unsigned int esr, struct pt_regs *regs);
asmlinkage void do_fpsimd_exc(unsigned int esr, struct pt_regs *regs);
asmlinkage void do_fpsimd_acc(unsigned int esr, struct pt_regs *regs);

#endif /* !ASM_FPSIMD_H */
