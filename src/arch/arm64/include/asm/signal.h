#ifndef ASM_SIGNAL_H
#define ASM_SIGNAL_H

void arch_show_signal(struct tcb *tsk, int signo, const char *str);

#endif /* !ASM_SIGNAL_H */
