/* SPDX-License-Identifier: GPL-2.0 */
#ifndef ASM_STACK_POINTER_H
#define ASM_STACK_POINTER_H

extern unsigned long current_stack_pointer;

/*
 * how to get the current stack pointer from C
 */
register unsigned long current_stack_pointer asm ("sp");

#endif /* !ASM_STACK_POINTER_H */
