/* SPDX-License-Identifier: GPL-2.0 */
#ifndef ASM_CURRENT_H
#define ASM_CURRENT_H

#include <utils/compiler.h>

#ifndef __ASSEMBLY__

struct tcb;

/*
 * We don't use read_sysreg() as we want the compiler to cache the value where
 * possible.
 */
static __always_inline struct tcb *get_current(void)
{
    unsigned long sp_el0;

    asm ("mrs %0, sp_el0" : "=r" (sp_el0));

    return (struct tcb *)sp_el0;
}

#define current get_current()

#endif /* __ASSEMBLY__ */
#endif /* !ASM_CURRENT_H */
