# SPDX-License-Identifier: Apache-2.0

add_custom_command(
  OUTPUT ${KERNEL_BINARY_DIR}/${KERNEL_BIN_NAME}
  COMMAND $<TARGET_PROPERTY:bintools,elfconvert_command>
    $<TARGET_PROPERTY:bintools,elfconvert_flag_outtarget>binary
    $<TARGET_PROPERTY:bintools,elfconvert_flag_section_remove>.note
    $<TARGET_PROPERTY:bintools,elfconvert_flag_section_remove>.note.gnu.build-id
    $<TARGET_PROPERTY:bintools,elfconvert_flag_section_remove>.comment
    $<TARGET_PROPERTY:bintools,elfconvert_flag_strip_all>
    $<TARGET_PROPERTY:bintools,elfconvert_flag_infile>${KERNEL_BINARY_DIR}/${KERNEL_ELF_NAME}
    $<TARGET_PROPERTY:bintools,elfconvert_flag_outfile>${KERNEL_BINARY_DIR}/${KERNEL_BIN_NAME}
    $<TARGET_PROPERTY:bintools,elfconvert_flag_final>
  DEPENDS
  kernel_final
)
add_custom_target(kernelbin DEPENDS ${KERNEL_BINARY_DIR}/${KERNEL_BIN_NAME})

set_property(
  TARGET kernelbin
  PROPERTY IMPORTED_LOCATION "${KERNEL_BINARY_DIR}/${KERNEL_BIN_NAME}"
)
