# SPDX-License-Identifier: Apache-2.0

kernel_library()

kernel_library_sources(
  head.S entry.S entry-fpsimd.S smccc-call.S smp.c process.c
  traps.c fpsimd.c cpu_ops.c smp_spin_table.c setup.c
  psci.c cpu_errata.c cpufeature.c insn.c cpuinfo.c
  irq.c time.c syscall.c signal.c sys.c ptrace.c
)
set_source_files_properties(
  head.S
  PROPERTIES COMPILE_DEFINITIONS "TEXT_OFFSET=${KERNEL_TEXT_OFFSET}"
)

set_property(GLOBAL PROPERTY KERNEL_LINKER_SCRIPT_S "${CMAKE_CURRENT_LIST_DIR}/linker.lds.S")
