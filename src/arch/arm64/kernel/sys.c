/*
 * AArch64-specific system calls implementation
 *
 * Copyright (C) 2012 ARM Ltd.
 * Author: Catalin Marinas <catalin.marinas@arm.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <seminix/kernel.h>
#include <seminix/syscall.h>

asmlinkage long sys_ni_syscall(const struct pt_regs *);
#define __arm64_sys_ni_syscall sys_ni_syscall

#undef __SYSCALL
#define __SYSCALL(nr, sym) asmlinkage long __arm64_##sym(const struct pt_regs *);
#include <asm/unistd.h>
#define __ARCH_WANT_SYS_POSIX
#include <asm/unistd.h>

#undef __SYSCALL
#define __SYSCALL(nr, sym)  [(-(nr)) - 1] = (syscall_fn_t)__arm64_##sym,

const syscall_fn_t seminix_sys_call_table[__NR_syscalls_seminix] = {
    [0 ... __NR_syscalls_seminix - 1] = (syscall_fn_t)sys_ni_syscall,
#include <asm/unistd.h>
};

#undef __SYSCALL
#define __SYSCALL(nr, sym)  [nr] = (syscall_fn_t)__arm64_##sym,

#define __ARCH_WANT_SYS_POSIX
const syscall_fn_t posix_sys_call_table[__NR_syscalls_posix] = {
    [0 ... __NR_syscalls_posix - 1] = (syscall_fn_t)sys_ni_syscall,
#include <asm/unistd.h>
};
