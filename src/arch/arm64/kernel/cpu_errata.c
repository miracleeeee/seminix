/*
 * Contains CPU specific errata definitions
 *
 * Copyright (C) 2014 ARM Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <utils/types.h>
#include <seminix/arm-smccc.h>
#include <seminix/psci.h>
#include <asm/cpu.h>
#include <asm/cputype.h>
#include <asm/cpufeature.h>

static bool __maybe_unused
is_affected_midr_range(const struct arm64_cpu_capabilities *entry, int scope)
{
    const struct arm64_midr_revidr *fix;
    u32 midr = read_cpuid_id(), revidr;

    WARN_ON(scope != SCOPE_LOCAL_CPU || preemptible());
    if (!is_midr_in_range(midr, &entry->midr_range))
        return false;

    midr &= MIDR_REVISION_MASK | MIDR_VARIANT_MASK;
    revidr = read_cpuid(REVIDR_EL1);
    for (fix = entry->fixed_revs; fix && fix->revidr_mask; fix++)
        if (midr == fix->midr_rv && (revidr & fix->revidr_mask))
            return false;

    return true;
}

static bool __maybe_unused
is_affected_midr_range_list(const struct arm64_cpu_capabilities *entry,
                int scope)
{
    WARN_ON(scope != SCOPE_LOCAL_CPU || preemptible());
    return is_midr_in_range_list(read_cpuid_id(), entry->midr_range_list);
}

static bool __maybe_unused
is_kryo_midr(const struct arm64_cpu_capabilities *entry, int scope)
{
    u32 model;

    WARN_ON(scope != SCOPE_LOCAL_CPU || preemptible());

    model = read_cpuid_id();
    model &= MIDR_IMPLEMENTOR_MASK | (0xf00 << MIDR_PARTNUM_SHIFT) |
         MIDR_ARCHITECTURE_MASK;

    return model == entry->midr_range.model;
}

static bool
has_mismatched_cache_type(const struct arm64_cpu_capabilities *entry,
              int scope)
{
    u64 mask = arm64_ftr_reg_ctrel0.strict_mask;
    u64 sys = arm64_ftr_reg_ctrel0.sys_val & mask;
    u64 ctr_raw, ctr_real;

    WARN_ON(scope != SCOPE_LOCAL_CPU || preemptible());

    /*
     * We want to make sure that all the CPUs in the system expose
     * a consistent CTR_EL0 to make sure that applications behaves
     * correctly with migration.
     *
     * If a CPU has CTR_EL0.IDC but does not advertise it via CTR_EL0 :
     *
     * 1) It is safe if the system doesn't support IDC, as CPU anyway
     *    reports IDC = 0, consistent with the rest.
     *
     * 2) If the system has IDC, it is still safe as we trap CTR_EL0
     *    access on this CPU via the ARM64_HAS_CACHE_IDC capability.
     *
     * So, we need to make sure either the raw CTR_EL0 or the effective
     * CTR_EL0 matches the system's copy to allow a secondary CPU to boot.
     */
    ctr_raw = read_cpuid_cachetype() & mask;
    ctr_real = read_cpuid_effective_cachetype() & mask;

    return (ctr_real != sys) && (ctr_raw != sys);
}

static void
cpu_enable_trap_ctr_access(const struct arm64_cpu_capabilities *__unused)
{
    u64 mask = arm64_ftr_reg_ctrel0.strict_mask;

    /* Trap CTR_EL0 access on this CPU, only if it has a mismatch */
    if ((read_cpuid_cachetype() & mask) !=
        (arm64_ftr_reg_ctrel0.sys_val & mask))
        sysreg_clear_set(sctlr_el1, SCTLR_EL1_UCT, 0);
}

static void __maybe_unused
cpu_enable_cache_maint_trap(const struct arm64_cpu_capabilities *__unused)
{
    sysreg_clear_set(sctlr_el1, SCTLR_EL1_UCI, 0);
}

#define CAP_MIDR_RANGE(model, v_min, r_min, v_max, r_max)	\
    .matches = is_affected_midr_range,			\
    .midr_range = MIDR_RANGE(model, v_min, r_min, v_max, r_max)

#define CAP_MIDR_ALL_VERSIONS(model)					\
    .matches = is_affected_midr_range,				\
    .midr_range = MIDR_ALL_VERSIONS(model)

#define MIDR_FIXED(rev, revidr_mask) \
    .fixed_revs = (struct arm64_midr_revidr[]){{ (rev), (revidr_mask) }, {}}

#define ERRATA_MIDR_RANGE(model, v_min, r_min, v_max, r_max)		\
    .type = ARM64_CPUCAP_LOCAL_CPU_ERRATUM,				\
    CAP_MIDR_RANGE(model, v_min, r_min, v_max, r_max)

#define CAP_MIDR_RANGE_LIST(list)				\
    .matches = is_affected_midr_range_list,			\
    .midr_range_list = list

/* Errata affecting a range of revisions of  given model variant */
#define ERRATA_MIDR_REV_RANGE(m, var, r_min, r_max)	 \
    ERRATA_MIDR_RANGE(m, var, r_min, var, r_max)

/* Errata affecting a single variant/revision of a model */
#define ERRATA_MIDR_REV(model, var, rev)	\
    ERRATA_MIDR_RANGE(model, var, rev, var, rev)

/* Errata affecting all variants/revisions of a given a model */
#define ERRATA_MIDR_ALL_VERSIONS(model)				\
    .type = ARM64_CPUCAP_LOCAL_CPU_ERRATUM,			\
    CAP_MIDR_ALL_VERSIONS(model)

/* Errata affecting a list of midr ranges, with same work around */
#define ERRATA_MIDR_RANGE_LIST(midr_list)			\
    .type = ARM64_CPUCAP_LOCAL_CPU_ERRATUM,			\
    CAP_MIDR_RANGE_LIST(midr_list)

#ifdef CONFIG_ARM64_WORKAROUND_REPEAT_TLBI

static const struct midr_range arm64_repeat_tlbi_cpus[] = {
#ifdef CONFIG_QCOM_FALKOR_ERRATUM_1009
    MIDR_RANGE(MIDR_QCOM_FALKOR_V1, 0, 0, 0, 0),
#endif
#ifdef CONFIG_ARM64_ERRATUM_1286807
    MIDR_RANGE(MIDR_CORTEX_A76, 0, 0, 3, 0),
#endif
    {},
};

#endif

#ifdef CONFIG_CAVIUM_ERRATUM_27456
static const struct midr_range cavium_erratum_27456_cpus[] = {
    /* Cavium ThunderX, T88 pass 1.x - 2.1 */
    MIDR_RANGE(MIDR_THUNDERX, 0, 0, 1, 1),
    /* Cavium ThunderX, T81 pass 1.0 */
    MIDR_REV(MIDR_THUNDERX_81XX, 0, 0),
    {},
};
#endif

#ifdef CONFIG_CAVIUM_ERRATUM_30115
static const struct midr_range cavium_erratum_30115_cpus[] = {
    /* Cavium ThunderX, T88 pass 1.x - 2.2 */
    MIDR_RANGE(MIDR_THUNDERX, 0, 0, 1, 2),
    /* Cavium ThunderX, T81 pass 1.0 - 1.2 */
    MIDR_REV_RANGE(MIDR_THUNDERX_81XX, 0, 0, 2),
    /* Cavium ThunderX, T83 pass 1.0 */
    MIDR_REV(MIDR_THUNDERX_83XX, 0, 0),
    {},
};
#endif

#ifdef CONFIG_QCOM_FALKOR_ERRATUM_1003
static const struct arm64_cpu_capabilities qcom_erratum_1003_list[] = {
    {
        ERRATA_MIDR_REV(MIDR_QCOM_FALKOR_V1, 0, 0),
    },
    {
        .midr_range.model = MIDR_QCOM_KRYO,
        .matches = is_kryo_midr,
    },
    {},
};
#endif

#ifdef CONFIG_ARM64_WORKAROUND_CLEAN_CACHE
static const struct midr_range workaround_clean_cache[] = {
#if	defined(CONFIG_ARM64_ERRATUM_826319) || \
    defined(CONFIG_ARM64_ERRATUM_827319) || \
    defined(CONFIG_ARM64_ERRATUM_824069)
    /* Cortex-A53 r0p[012]: ARM errata 826319, 827319, 824069 */
    MIDR_REV_RANGE(MIDR_CORTEX_A53, 0, 0, 2),
#endif
#ifdef	CONFIG_ARM64_ERRATUM_819472
    /* Cortex-A53 r0p[01] : ARM errata 819472 */
    MIDR_REV_RANGE(MIDR_CORTEX_A53, 0, 0, 1),
#endif
    {},
};
#endif

const struct arm64_cpu_capabilities arm64_errata[] = {
#ifdef CONFIG_ARM64_WORKAROUND_CLEAN_CACHE
    {
        .desc = "ARM errata 826319, 827319, 824069, 819472",
        .capability = ARM64_WORKAROUND_CLEAN_CACHE,
        ERRATA_MIDR_RANGE_LIST(workaround_clean_cache),
        .cpu_enable = cpu_enable_cache_maint_trap,
    },
#endif
#ifdef CONFIG_ARM64_ERRATUM_832075
    {
    /* Cortex-A57 r0p0 - r1p2 */
        .desc = "ARM erratum 832075",
        .capability = ARM64_WORKAROUND_DEVICE_LOAD_ACQUIRE,
        ERRATA_MIDR_RANGE(MIDR_CORTEX_A57,
                  0, 0,
                  1, 2),
    },
#endif
#ifdef CONFIG_ARM64_ERRATUM_834220
    {
    /* Cortex-A57 r0p0 - r1p2 */
        .desc = "ARM erratum 834220",
        .capability = ARM64_WORKAROUND_834220,
        ERRATA_MIDR_RANGE(MIDR_CORTEX_A57,
                  0, 0,
                  1, 2),
    },
#endif
#ifdef CONFIG_ARM64_ERRATUM_843419
    {
    /* Cortex-A53 r0p[01234] */
        .desc = "ARM erratum 843419",
        .capability = ARM64_WORKAROUND_843419,
        ERRATA_MIDR_REV_RANGE(MIDR_CORTEX_A53, 0, 0, 4),
        MIDR_FIXED(0x4, BIT(8)),
    },
#endif
#ifdef CONFIG_ARM64_ERRATUM_845719
    {
    /* Cortex-A53 r0p[01234] */
        .desc = "ARM erratum 845719",
        .capability = ARM64_WORKAROUND_845719,
        ERRATA_MIDR_REV_RANGE(MIDR_CORTEX_A53, 0, 0, 4),
    },
#endif
#ifdef CONFIG_CAVIUM_ERRATUM_23154
    {
    /* Cavium ThunderX, pass 1.x */
        .desc = "Cavium erratum 23154",
        .capability = ARM64_WORKAROUND_CAVIUM_23154,
        ERRATA_MIDR_REV_RANGE(MIDR_THUNDERX, 0, 0, 1),
    },
#endif
#ifdef CONFIG_CAVIUM_ERRATUM_27456
    {
        .desc = "Cavium erratum 27456",
        .capability = ARM64_WORKAROUND_CAVIUM_27456,
        ERRATA_MIDR_RANGE_LIST(cavium_erratum_27456_cpus),
    },
#endif
#ifdef CONFIG_CAVIUM_ERRATUM_30115
    {
        .desc = "Cavium erratum 30115",
        .capability = ARM64_WORKAROUND_CAVIUM_30115,
        ERRATA_MIDR_RANGE_LIST(cavium_erratum_30115_cpus),
    },
#endif
    {
        .desc = "Mismatched cache type (CTR_EL0)",
        .capability = ARM64_MISMATCHED_CACHE_TYPE,
        .matches = has_mismatched_cache_type,
        .type = ARM64_CPUCAP_LOCAL_CPU_ERRATUM,
        .cpu_enable = cpu_enable_trap_ctr_access,
    },
#ifdef CONFIG_QCOM_FALKOR_ERRATUM_1003
    {
        .desc = "Qualcomm Technologies Falkor/Kryo erratum 1003",
        .capability = ARM64_WORKAROUND_QCOM_FALKOR_E1003,
        .matches = cpucap_multi_entry_cap_matches,
        .match_list = qcom_erratum_1003_list,
    },
#endif
#ifdef CONFIG_ARM64_WORKAROUND_REPEAT_TLBI
    {
        .desc = "Qualcomm erratum 1009, ARM erratum 1286807",
        .capability = ARM64_WORKAROUND_REPEAT_TLBI,
        ERRATA_MIDR_RANGE_LIST(arm64_repeat_tlbi_cpus),
    },
#endif
#ifdef CONFIG_ARM64_ERRATUM_858921
    {
    /* Cortex-A73 all versions */
        .desc = "ARM erratum 858921",
        .capability = ARM64_WORKAROUND_858921,
        ERRATA_MIDR_ALL_VERSIONS(MIDR_CORTEX_A73),
    },
#endif
#ifdef CONFIG_HARDEN_BRANCH_PREDICTOR
    {
        .capability = ARM64_HARDEN_BRANCH_PREDICTOR,
        .cpu_enable = enable_smccc_arch_workaround_1,
        ERRATA_MIDR_RANGE_LIST(arm64_bp_harden_smccc_cpus),
    },
#endif
#ifdef CONFIG_HARDEN_EL2_VECTORS
    {
        .desc = "EL2 vector hardening",
        .capability = ARM64_HARDEN_EL2_VECTORS,
        ERRATA_MIDR_RANGE_LIST(arm64_harden_el2_vectors),
    },
#endif
#ifdef CONFIG_ARM64_SSBD
    {
        .desc = "Speculative Store Bypass Disable",
        .capability = ARM64_SSBD,
        .type = ARM64_CPUCAP_LOCAL_CPU_ERRATUM,
        .matches = has_ssbd_mitigation,
    },
#endif
#ifdef CONFIG_ARM64_ERRATUM_1188873
    {
        /* Cortex-A76 r0p0 to r2p0 */
        .desc = "ARM erratum 1188873",
        .capability = ARM64_WORKAROUND_1188873,
        ERRATA_MIDR_RANGE(MIDR_CORTEX_A76, 0, 0, 2, 0),
    },
#endif
#ifdef CONFIG_ARM64_ERRATUM_1165522
    {
        /* Cortex-A76 r0p0 to r2p0 */
        .desc = "ARM erratum 1165522",
        .capability = ARM64_WORKAROUND_1165522,
        ERRATA_MIDR_RANGE(MIDR_CORTEX_A76, 0, 0, 2, 0),
    },
#endif
    {
    }
};
