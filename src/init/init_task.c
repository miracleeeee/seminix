#include <seminix/sched.h>
#include <seminix/tcb.h>
#include <seminix/mm.h>

struct tcb init_task = {
    .thread_info    = INIT_THREAD_INFO(init_task),
    .comm       = INIT_TASK_COMM,
    .tid        = 0,
    .flags      = 0,
    .usage		= ATOMIC_INIT(1),
    .pi_lock    = __RAW_SPIN_LOCK_UNLOCKED(init_task.pi_lock),
    .state      = TASK_RUNNING,
    .mm		    = &init_mm,
    .stack          = init_stack,
    .stack_refcount = ATOMIC_INIT(1),
    .thread     = INIT_THREAD,
};
