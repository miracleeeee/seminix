#ifndef CAP_CAP_TYPEDEF_H
#define CAP_CAP_TYPEDEF_H

#define CAP_REF(p)  (&((p)->cap))

#define CAP_TO_CAPPTR(cap, cap_type, cap_struct)    \
    ({ assert(cap_get_cap_type(cap) == cap_type); (cap_struct *)(cap); })

#define CAP_CNODE_PTR(cap)    CAP_TO_CAPPTR(cap, cap_cnode_cap, cap_cnode_t)
#define CAP_RLIMIT_PTR(cap)   CAP_TO_CAPPTR(cap, cap_rlimit_cap, cap_rlimit_t)
#define CAP_ENDPOINT_PTR(cap) CAP_TO_CAPPTR(cap, cap_endpoint_cap, cap_endpoint_t)
#define CAP_FRAME_PTR(cap)    CAP_TO_CAPPTR(cap, cap_frame_cap, cap_frame_t)
#define CAP_THREAD_PTR(cap)   CAP_TO_CAPPTR(cap, cap_thread_cap, cap_thread_t)
#define CAP_VSPACE_PTR(cap)   CAP_TO_CAPPTR(cap, cap_vspace_cap, cap_vspace_t)

#endif /* !CAP_CAP_TYPEDEF_H */
