#ifndef CAP_IRQ_H
#define CAP_IRQ_H

#include <cap/cap.h>

typedef struct irq {
    cap_t cap;
    int virq;
} irq_t;

#endif /* !CAP_IRQ_H */
