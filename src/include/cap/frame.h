#ifndef CAP_FRAME_H
#define CAP_FRAME_H

#include <utils/pagesize.h>
#include <seminix/mm.h>
#include <cap/cap.h>

typedef struct frame {
    union {
        struct {
            phys_addr_t ioaddr;
            unsigned int order;
        } mmio;
        struct {
            struct page **pages;
            unsigned int nr_pages;
            unsigned int order;
        } page;
    };
#define FRAMETYPE_MEMIO     1
#define FRAMETYPE_IOPORT    2
#define FRAMETYPE_PAGE      3
    int frame_type;
} frame_t;

typedef struct cap_frame {
    cap_t cap;
    frame_t *frame;
} cap_frame_t;

extern const struct cap_ops frame_cap_ops;

#endif /* !CAP_FRAME_H */
