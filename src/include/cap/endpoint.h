#ifndef CAP_ENDPOINT_H
#define CAP_ENDPOINT_H

#include <cap/cap.h>
#include <seminix/wait.h>

typedef struct endpoint {
    struct list_head recv_list;
    struct list_head send_list;
    struct list_head reply_list;
    struct seminix_ipc_notify notify[IPC_NOTIFY_MAX];
    seminix_message_t irqnotify[IPC_IRQ_NOTIFY_MAX];
    int notify_cur, irqnotify_cur;
    spinlock_t  lock;
} endpoint_t;

typedef struct cap_endpoint {
    cap_t cap;
    unsigned long badge;
    int status;
    struct list_head list;
    wait_queue_head_t wait;
    struct tcb *tsk;
    endpoint_t *endpoint;
} cap_endpoint_t;

extern const struct cap_ops endpoint_cap_ops;

#endif /* !CAP_ENDPOINT_H */
