#ifndef CAP_IPC_BUFFER_H
#define CAP_IPC_BUFFER_H

#include <utils/types.h>
#include <seminix/tcb.h>

static inline void set_task_message(struct tcb *tsk, seminix_message_t message)
{
    BUG_ON(!tsk->ipc_buffer);

    tsk->ipc_buffer->message = message;
}

static inline seminix_message_t get_task_message(struct tcb *tsk)
{
    BUG_ON(!tsk->ipc_buffer);

    return tsk->ipc_buffer->message;
}

static inline void set_task_mr(struct tcb *tsk, int index, uintptr_t msg)
{
    BUG_ON(!tsk->ipc_buffer);
    BUG_ON(index >= (int)SEMINIX_MSG_LENGTH_MAX);

    tsk->ipc_buffer->msg[index] = msg;
}

static inline uintptr_t get_task_mr(struct tcb *tsk, int index)
{
    BUG_ON(!tsk->ipc_buffer);
    BUG_ON(index >= (int)SEMINIX_MSG_LENGTH_MAX);

    return tsk->ipc_buffer->msg[index];
}

static inline void set_task_badge(struct tcb *tsk, unsigned long badge)
{
    BUG_ON(!tsk->ipc_buffer);

    tsk->ipc_buffer->badge = badge;
}

static inline unsigned long get_task_badge(struct tcb *tsk)
{
    return tsk->ipc_buffer->badge;
}

static inline void set_task_reply(struct tcb *tsk, bool reply)
{
    BUG_ON(!tsk->ipc_buffer);

    tsk->ipc_buffer->reply = reply;
}

static inline bool get_task_reply(struct tcb *tsk)
{
    BUG_ON(!tsk->ipc_buffer);

    return tsk->ipc_buffer->reply;
}

static inline void set_task_capdesc(struct tcb *tsk, int index, int cd)
{
    tsk->ipc_buffer->capdesc[index] = cd;
}

static inline int get_task_capdesc(struct tcb *tsk, int index)
{
    return tsk->ipc_buffer->capdesc[index];
}

static inline void set_task_notify(struct tcb *tsk, int index, unsigned long badge,
    seminix_message_t message)
{
    tsk->ipc_buffer->notify[index].badge = badge;
    tsk->ipc_buffer->notify[index].message = message;
}

static inline struct seminix_ipc_notify get_task_notify(struct tcb *tsk, int index)
{
    return tsk->ipc_buffer->notify[index];
}

static inline void set_task_irqnotify(struct tcb *tsk, int index, seminix_message_t message)
{
    tsk->ipc_buffer->irqnotify[index] = message;
}

static inline seminix_message_t get_task_irqnotify(struct tcb *tsk, int index)
{
    return tsk->ipc_buffer->irqnotify[index];
}

/*================= set current task ===================*/

static inline void set_current_message(seminix_message_t message)
{
    set_task_message(current, message);
}

static inline seminix_message_t get_current_message(void)
{
    return get_task_message(current);
}

static inline void set_current_mr(int index, uintptr_t msg)
{
    set_task_mr(current, index, msg);
}

static inline uintptr_t get_current_mr(int index)
{
    return get_task_mr(current, index);
}

static inline void set_current_badge(unsigned long badge)
{
    set_task_badge(current, badge);
}

static inline unsigned long get_current_badge(void)
{
    return get_task_badge(current);
}

static inline void set_current_reply(bool reply)
{
    set_task_reply(current, reply);
}

static inline bool get_current_reply(void)
{
    return get_task_reply(current);
}

static inline void set_current_capdesc(int index, int cd)
{
    set_task_capdesc(current, index, cd);
}

static inline int get_current_capdesc(int index)
{
    return get_task_capdesc(current, index);
}

static inline void set_current_notify(int index, unsigned long badge,
    seminix_message_t message)
{
    set_task_notify(current, index, badge, message);
}

static inline struct seminix_ipc_notify get_current_notify(int index)
{
    return get_task_notify(current, index);
}

static inline void set_current_irqnotify(int index, seminix_message_t message)
{
    set_task_irqnotify(current, index, message);
}

static inline seminix_message_t get_current_irqnotify(int index)
{
    return get_task_irqnotify(current, index);
}

#endif /* !CAP_IPC_BUFFER_H */
