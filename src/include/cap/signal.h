#ifndef CAP_SIGNAL_H
#define CAP_SIGNAL_H

#include <cap/cap.h>

typedef struct cap_signal {
    cap_t cap;
} cap_signal_t;

extern const struct cap_ops signal_cap_ops;

#endif /* !CAP_SIGNAL_H */
