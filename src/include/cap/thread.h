#ifndef CAP_THREAD_H
#define CAP_THREAD_H

#include <cap/cap.h>

struct tcb;

typedef struct cap_thread {
    cap_t cap;
    struct tcb *task;
} cap_thread_t;

extern const struct cap_ops thread_cap_ops;

void thread_suspend(struct tcb *tsk);
bool thread_resume(struct tcb *tsk);

#endif /* !CAP_THREAD_H */
