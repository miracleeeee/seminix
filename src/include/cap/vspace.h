#ifndef CAP_VSPACE_H
#define CAP_VSPACE_H

#include <seminix/mm.h>
#include <cap/cap.h>

typedef struct cap_vspace {
    cap_t cap;
    struct mm_struct *mm;
} cap_vspace_t;

extern const struct cap_ops vspace_cap_ops;





#endif /* !CAP_VSPACE_H */
