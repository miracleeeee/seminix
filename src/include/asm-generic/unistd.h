/* SPDX-License-Identifier: GPL-2.0 WITH Linux-syscall-note */

#ifndef __SYSCALL
#define __SYSCALL(x, y)
#endif

#ifndef __ARCH_WANT_SYS_POSIX
#include <asm-generic/unistd-seminix.h>
#else
#include <asm-generic/unistd-posix.h>
#endif /* !__ARCH_WANT_SYS_POSIX */

#undef __NR_syscalls_seminix
#define __NR_syscalls_seminix __NR_seminix_MAX
#undef __NR_syscalls_posix
#define __NR_syscalls_posix  295

#undef __ARCH_WANT_SYS_POSIX
