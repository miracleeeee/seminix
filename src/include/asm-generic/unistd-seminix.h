/* SPDX-License-Identifier: GPL-2.0 WITH Linux-syscall-note */

#include <libseminix/syscalls_types.h>

/* kernel/cap/rlimit.c */
__SYSCALL(__NR_seminix_CreateRlimit, sys_createrlimit)
__SYSCALL(__NR_seminix_SetRlimit, sys_setrlimit)
__SYSCALL(__NR_seminix_GetRlimit, sys_getrlimit)

/* kernel/cap/cnode.c */
__SYSCALL(__NR_seminix_CNodeExpand, sys_cnode_expand)
__SYSCALL(__NR_seminix_CNodeDup, sys_cnode_dup)
__SYSCALL(__NR_seminix_CNodeMove, sys_cnode_move)
__SYSCALL(__NR_seminix_CNodeRevoke, sys_cnode_revoke)
__SYSCALL(__NR_seminix_CNodeDelete, sys_cnode_delete)

__SYSCALL(__NR_seminix_TCBWakeUpNew, sys_tcb_wake_up_new)
__SYSCALL(__NR_seminix_TCBConfigure, sys_tcb_configure)
/* __NR_seminix_TCBPriority is not set */
__SYSCALL(__NR_seminix_TCBControl, sys_tcb_control)
__SYSCALL(__NR_seminix_Yield, sys_tcb_yield)

/* __NR_seminix_DeviceControl is not set */
/* __NR_seminix_Device is not set */
/* __NR_seminix_DeviceIRQ is not set */

/* __NR_seminix_SchedDeadline is not set */

/* __NR_seminix_VMMAP is not set */
/* __NR_seminix_VMProtModify is not set */
/* __NR_seminix_VMUNMAP is not set */
/* __NR_seminix_VMDUP is not set */
/* __NR_seminix_VMAGET is not set */
/* __NR_seminix_VMCOPY is not set */

/* __NR_seminix_SetBadge is not set */
/* __NR_seminix_SendIPC is not set */
/* __NR_seminix_RecvIPC is not set */
/* __NR_seminix_ReplyIPC is not set */

/* arch define (signal.c) */
/* __NR_seminix_SetSignal is not set */
/* __NR_seminix_ClearSignal is not set */
/* __NR_seminix_SendSignal is not set */
__SYSCALL(__NR_seminix_SignalReturn, sys_rt_sigreturn)

__SYSCALL(__NR_seminix_SysCtl, sys_sysctl)

/* __NR_seminix_restart_syscall is not set */
