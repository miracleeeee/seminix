/* SPDX-License-Identifier: GPL-2.0 WITH Linux-syscall-note */

#include <bits/syscall.h>

/* __NR_io_setup is not set */
/* __NR_io_destroy is not set */
/* __NR_io_submit is not set */
/* __NR_io_cancel is not set */
/* __NR_io_getevents is not set */

/* __NR_setxattr is not set */
/* __NR_lsetxattr is not set */
/* __NR_fsetxattr is not set */
/* __NR_getxattr is not set */
/* __NR_lgetxattr is not set */
/* __NR_fgetxattr is not set */
/* __NR_listxattr is not set */
/* __NR_llistxattr is not set */
/* __NR_flistxattr is not set */
/* __NR_removexattr is not set */
/* __NR_lremovexattr is not set */
/* __NR_fremovexattr is not set */

/* __NR_getcwd is not set */

/* __NR_lookup_dcookie is not set */

/* __NR_eventfd2 is not set */

/* __NR_epoll_create1 is not set */
/* __NR_epoll_ctl is not set */
/* __NR_epoll_pwait is not set */

/* __NR_dup is not set */
/* __NR_dup3 is not set */
/* __NR_fcntl is not set */

/* __NR_inotify_init1 is not set */
/* __NR_inotify_add_watch is not set */
/* __NR_inotify_rm_watch is not set */

__SYSCALL(__NR_ioctl, sys_ioctl)

/* __NR_ioprio_set is not set */
/* __NR_ioprio_get is not set */

/* __NR_flock is not set */

/* __NR_mknodat is not set */
/* __NR_mkdirat is not set */
/* __NR_unlinkat is not set */
/* __NR_symlinkat is not set */
/* __NR_linkat is not set */
/* __NR_renameat is not set */

/* __NR_umount2 is not set */
/* __NR_mount is not set */
/* __NR_pivot_root is not set */

/* __NR_nfsservctl is not set */

/* __NR_statfs is not set */
/* __NR_fstatfs is not set */
/* __NR_truncate is not set */
/* __NR_ftruncate is not set */

/* __NR_fallocate is not set */
/* __NR_faccessat is not set */
/* __NR_chdir is not set */
/* __NR_fchdir is not set */
/* __NR_chroot is not set */
/* __NR_fchmod is not set */
/* __NR_fchmodat is not set */
/* __NR_fchownat is not set */
/* __NR_fchown is not set */
/* __NR_openat is not set */
/* __NR_close is not set */
/* __NR_vhangup is not set */

/* __NR_pipe2 is not set */

/* __NR_quotactl is not set */

/* __NR_getdents64 is not set */

/* __NR_lseek is not set */
/* __NR_read is not set */
/* __NR_write is not set */
/* __NR_readv is not set */
__SYSCALL(__NR_writev, sys_writev)
/* __NR_pread64 is not set */
/* __NR_pwrite64 is not set */
/* __NR_preadv is not set */
/* __NR_pwritev is not set */

/* __NR_sendfile is not set */

/* __NR_pselect6 is not set */
/* __NR_ppoll is not set */

/* __NR_signalfd4 is not set */

/* __NR_vmsplice is not set */
/* __NR_splice is not set */
/* __NR_tee is not set */

/* __NR_readlinkat is not set */
/* __NR_newfstatat is not set */
/* __NR_fstat is not set */

/* __NR_sync is not set */
/* __NR_fsync is not set */
/* __NR_fdatasync is not set */
/* __NR_sync_file_range is not set */

/* __NR_timerfd_create is not set */
/* __NR_timerfd_settime is not set */
/* __NR_timerfd_gettime is not set */

/* __NR_utimensat is not set */

/* __NR_acct is not set */

/* __NR_capget is not set */
/* __NR_capset is not set */

/* __NR_personality is not set */

/* __NR_exit is not set */
/* __NR_exit_group is not set */
/* __NR_waitid is not set */

__SYSCALL(__NR_set_tid_address, sys_set_tid_address)
/* __NR_unshare is not set */

__SYSCALL(__NR_futex, sys_futex)
/* __NR_set_robust_list is not set */
/* __NR_get_robust_list is not set */

/* __NR_nanosleep is not set */

/* __NR_getitimer is not set */
/* __NR_setitimer is not set */

/* __NR_kexec_load is not set */

/* __NR_init_module is not set */
/* __NR_delete_module is not set */

/* __NR_timer_create is not set */
/* __NR_timer_gettime is not set */
/* __NR_timer_getoverrun is not set */
/* __NR_timer_settime is not set */
/* __NR_timer_delete is not set */
/* __NR_clock_settime is not set */
/* __NR_clock_gettime is not set */
/* __NR_clock_getres is not set */
/* __NR_clock_nanosleep is not set */

/* __NR_syslog is not set */

/* __NR_ptrace is not set */

/* __NR_sched_setparam is not set */
/* __NR_sched_setscheduler is not set */
/* __NR_sched_getscheduler is not set */
/* __NR_sched_getparam is not set */
/* __NR_sched_setaffinity is not set */
/* __NR_sched_getaffinity is not set */
/* __NR_sched_yield is not set */
/* __NR_sched_get_priority_max is not set */
/* __NR_sched_get_priority_min is not set */
/* __NR_sched_rr_get_interval is not set */

/* __NR_restart_syscall is not set */
/* __NR_kill is not set */
/* __NR_tkill is not set */
/* __NR_tgkill is not set */
/* __NR_sigaltstack is not set */
/* __NR_rt_sigsuspend is not set */
/* __NR_rt_sigaction is not set */
__SYSCALL(__NR_rt_sigprocmask, sys_rt_sigprocmask)
/* __NR_rt_sigpending is not set */
/* __NR_rt_sigtimedwait is not set */
/* __NR_rt_sigqueueinfo is not set */
/* __NR_rt_sigreturn is not set */

/* __NR_setpriority is not set */
/* __NR_getpriority is not set */
/* __NR_reboot is not set */
/* __NR_setregid is not set */
/* __NR_setgid is not set */
/* __NR_setreuid is not set */
/* __NR_setuid is not set */
/* __NR_setresuid is not set */
/* __NR_getresuid is not set */
/* __NR_setresgid is not set */
/* __NR_getresgid is not set */
/* __NR_setfsuid is not set */
/* __NR_setfsgid is not set */
/* __NR_times is not set */
/* __NR_setpgid is not set */
/* __NR_getpgid is not set */
/* __NR_getsid is not set */
/* __NR_setsid is not set */
/* __NR_getgroups is not set */
/* __NR_setgroups is not set */
/* __NR_uname is not set */
/* __NR_sethostname is not set */
/* __NR_setdomainname is not set */
/* __NR_getrlimit is not set */
/* __NR_setrlimit is not set */
/* __NR_getrusage is not set */
/* __NR_umask is not set */
/* __NR_prctl is not set */
/* __NR_getcpu is not set */

/* __NR_gettimeofday is not set */
/* __NR_settimeofday is not set */
/* __NR_adjtimex is not set */

/* __NR_getpid is not set */
/* __NR_getppid is not set */
/* __NR_getuid is not set */
/* __NR_geteuid is not set */
/* __NR_getgid is not set */
/* __NR_getegid is not set */
/* __NR_gettid is not set */
/* __NR_sysinfo is not set */

/* __NR_mq_open is not set */
/* __NR_mq_unlink is not set */
/* __NR_mq_timedsend is not set */
/* __NR_mq_timedreceive is not set */
/* __NR_mq_notify is not set */
/* __NR_mq_getsetattr is not set */

/* __NR_msgget is not set */
/* __NR_msgctl is not set */
/* __NR_msgrcv is not set */
/* __NR_msgsnd is not set */

/* __NR_semget is not set */
/* __NR_semctl is not set */
/* __NR_semtimedop is not set */
/* __NR_semop is not set */

/* __NR_shmget is not set */
/* __NR_shmctl is not set */
/* __NR_shmat is not set */
/* __NR_shmdt is not set */

/* __NR_socket is not set */
/* __NR_socketpair is not set */
/* __NR_bind is not set */
/* __NR_listen is not set */
/* __NR_accept is not set */
/* __NR_connect is not set */
/* __NR_getsockname is not set */
/* __NR_getpeername is not set */
/* __NR_sendto is not set */
/* __NR_recvfrom is not set */
/* __NR_setsockopt is not set */
/* __NR_getsockopt is not set */
/* __NR_shutdown is not set */
/* __NR_sendmsg is not set */
/* __NR_recvmsg is not set */

/* __NR_readahead is not set */

/* __NR_brk is not set */
/* __NR_munmap is not set */
/* __NR_mremap is not set */

/* __NR_add_key is not set */
/* __NR_request_key is not set */
/* __NR_keyctl is not set */

__SYSCALL(__NR_clone, sys_clone)
/* __NR_execve is not set */

__SYSCALL(__NR_mmap, sys_mmap)

/* __NR_fadvise64 is not set */

/* __NR_swapon is not set */
/* __NR_swapoff is not set */
/* __NR_mprotect is not set */
/* __NR_msync is not set */
/* __NR_mlock is not set */
/* __NR_munlock is not set */
/* __NR_mlockall is not set */
/* __NR_munlockall is not set */
/* __NR_mincore is not set */
/* __NR_madvise is not set */
/* __NR_remap_file_pages is not set */
/* __NR_mbind is not set */
/* __NR_get_mempolicy is not set */
/* __NR_set_mempolicy is not set */
/* __NR_migrate_pages is not set */
/* __NR_move_pages is not set */

/* __NR_rt_tgsigqueueinfo is not set */
/* __NR_perf_event_open is not set */
/* __NR_accept4 is not set */
/* __NR_recvmmsg is not set */

/* __NR_wait4 is not set */
/* __NR_prlimit64 is not set */
/* __NR_fanotify_init is not set */
/* __NR_fanotify_mark is not set */
/* __NR_name_to_handle_at is not set */
/* __NR_open_by_handle_at is not set */
/* __NR_clock_adjtime is not set */
/* __NR_syncfs is not set */
/* __NR_setns is not set */
/* __NR_sendmmsg is not set */
/* __NR_process_vm_readv is not set */
/* __NR_process_vm_writev is not set */
/* __NR_kcmp is not set */
/* __NR_finit_module is not set */
/* __NR_sched_setattr is not set */
/* __NR_sched_getattr is not set */
/* __NR_renameat2 is not set */
/* __NR_seccomp is not set */
/* __NR_getrandom is not set */
/* __NR_memfd_create is not set */
/* __NR_bpf is not set */
/* __NR_execveat is not set */
/* __NR_userfaultfd is not set */
/* __NR_membarrier is not set */
/* __NR_mlock2 is not set */
/* __NR_copy_file_range is not set */
/* __NR_preadv2 is not set */
/* __NR_pwritev2 is not set */
/* __NR_pkey_mprotect is not set */
/* __NR_pkey_alloc is not set */
/* __NR_pkey_free is not set */
/* __NR_statx is not set */
/* __NR_io_pgetevents is not set */
/* __NR_rseq is not set */
/* __NR_kexec_file_load is not set */
/* __NR_pidfd_send_signal is not set */
/* __NR_io_uring_setup is not set */
/* __NR_io_uring_enter is not set */
/* __NR_io_uring_register is not set */
/* __NR_open_tree is not set */
/* __NR_move_mount is not set */
/* __NR_fsopen is not set */
/* __NR_fsconfig is not set */
/* __NR_fsmount is not set */
/* __NR_fspick is not set */
/* __NR_pidfd_open is not set */
/* __NR_clone3 is not set */
/* __NR_close_range is not set */
/* __NR_openat2 is not set */
/* __NR_pidfd_getfd is not set */
/* __NR_faccessat2 is not set */
/* __NR_process_madvise is not set */
/* __NR_epoll_pwait2 is not set */
/* __NR_mount_setattr is not set */
/* __NR_landlock_create_ruleset is not set */
/* __NR_landlock_add_rule is not set */
/* __NR_landlock_restrict_self is not set */
