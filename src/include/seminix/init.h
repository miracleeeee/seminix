#ifndef SEMINIX_INIT_H
#define SEMINIX_INIT_H

#include <utils/compiler.h>

#define __init		__section(.init.text) __cold __latent_entropy
#define __initdata	__section(.init.data)
#define __initconst __section(.init.rodata)

#define __HEAD		.section	".head.text","ax"
#define __INIT		.section	".init.text","ax"
#define __FINIT		.previous

#define __INITDATA	.section	".init.data","aw",%progbits
#define __INITRODATA	.section	".init.rodata","a",%progbits
#define __FINITDATA	.previous

#endif /* !SEMINIX_INIT_H */
