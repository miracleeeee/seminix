#ifndef SEMINIX_THREAD_H
#define SEMINIX_THREAD_H

#include <asm/thread.h>
#include <asm/current.h>

#define current_thread_info() ((struct thread_info *)current)

#ifndef THREAD_ALIGN
#define THREAD_ALIGN	THREAD_SIZE
#endif

static inline void set_ti_thread_flag(struct thread_info *ti, int flag)
{
    set_bit(flag, (unsigned long *)&ti->flags);
}

static inline void clear_ti_thread_flag(struct thread_info *ti, int flag)
{
    clear_bit(flag, (unsigned long *)&ti->flags);
}

static inline void update_ti_thread_flag(struct thread_info *ti, int flag,
                     bool value)
{
    if (value)
        set_ti_thread_flag(ti, flag);
    else
        clear_ti_thread_flag(ti, flag);
}

static inline int test_and_set_ti_thread_flag(struct thread_info *ti, int flag)
{
    return test_and_set_bit(flag, (unsigned long *)&ti->flags);
}

static inline int test_and_clear_ti_thread_flag(struct thread_info *ti, int flag)
{
    return test_and_clear_bit(flag, (unsigned long *)&ti->flags);
}

static inline int test_ti_thread_flag(struct thread_info *ti, int flag)
{
    return test_bit(flag, (unsigned long *)&ti->flags);
}

#define set_thread_flag(flag) \
    set_ti_thread_flag(current_thread_info(), flag)
#define clear_thread_flag(flag) \
    clear_ti_thread_flag(current_thread_info(), flag)
#define update_thread_flag(flag, value) \
    update_ti_thread_flag(current_thread_info(), flag, value)
#define test_and_set_thread_flag(flag) \
    test_and_set_ti_thread_flag(current_thread_info(), flag)
#define test_and_clear_thread_flag(flag) \
    test_and_clear_ti_thread_flag(current_thread_info(), flag)
#define test_thread_flag(flag) \
    test_ti_thread_flag(current_thread_info(), flag)

#define tif_need_resched() test_thread_flag(TIF_NEED_RESCHED)

#endif /* !SEMINIX_THREAD_H */
