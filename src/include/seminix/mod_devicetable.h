/* SPDX-License-Identifier: GPL-2.0 */
/*
 * Device tables which are exported to userspace via
 * scripts/mod/file2alias.c.  You must keep that file in sync with this
 * header.
 */
#ifndef SEMINIX_MOD_DEVICETABLE_H
#define SEMINIX_MOD_DEVICETABLE_H

#include <libseminix/types.h>

/*
 * Struct used for matching a device
 */
struct of_device_id {
    char	name[32];
    char	type[32];
    char	compatible[DEVICE_COMPATIBLE_MAX];
    const void *data;
};

#endif /* !SEMINIX_MOD_DEVICETABLE_H */
