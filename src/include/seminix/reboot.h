/* SPDX-License-Identifier: GPL-2.0 */
#ifndef SEMINIX_REBOOT_H
#define SEMINIX_REBOOT_H

enum reboot_mode {
    REBOOT_COLD = 0,
    REBOOT_WARM,
    REBOOT_HARD,
    REBOOT_SOFT,
    REBOOT_GPIO,
};
extern enum reboot_mode reboot_mode;

extern void machine_halt(void);
extern void machine_power_off(void);
extern void machine_restart(char *cmd);

void kernel_restart(char *cmd);
void kernel_halt(void);
void kernel_power_off(void);

#endif /* !SEMINIX_REBOOT_H */
