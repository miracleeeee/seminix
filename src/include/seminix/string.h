#ifndef SEMINIX_STRING_H
#define SEMINIX_STRING_H

#include <seminix/slab.h>
#include <asm/string.h>

#ifndef __ARCH_HAVE_STRNCHR
extern char * strnchr(const char *, size_t, int);
#endif

#ifndef __ARCH_HAVE_MEMSCAN
extern void * memscan(void *, int, size_t);
#endif

#ifndef __ARCH_HAVE_STRNSTR
extern char * strnstr(const char *, const char *, size_t);
#endif

extern __printf(3, 0) int vscnprintf(char *buf, size_t size, const char *fmt, va_list args);
extern __printf(3, 4) int scnprintf(char *buf, size_t size, const char *fmt, ...);

extern __printf(2, 3) __malloc char *kasprintf(gfp_t gfp, const char *fmt, ...);
extern __printf(2, 0) __malloc char *kvasprintf(gfp_t gfp, const char *fmt, va_list args);
extern char *kstrdup(const char *s, gfp_t gfp) __malloc;

#endif /* !SEMINIX_STRING_H */
