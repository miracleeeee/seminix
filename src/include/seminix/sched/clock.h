/* SPDX-License-Identifier: GPL-2.0 */
#ifndef SEMINIX_SCHED_SCHED_CLOCK_H
#define SEMINIX_SCHED_SCHED_CLOCK_H

#include <utils/types.h>
#include <seminix/sched_clock.h>

void sched_clock_init(void);

u64 sched_clock_cpu(int cpu);

static inline u64 local_clock(void)
{
    return sched_clock();
}

#endif /* !SEMINIX_SCHED_SCHED_CLOCK_H */
