#ifndef SEMINIX_SCHED_INIT_TASK_H
#define SEMINIX_SCHED_INIT_TASK_H

#include <seminix/thread.h>

#define __init_task_data __attribute__((__section__(".data..init_task")))

#define INIT_TASK_COMM  "idle"

extern struct tcb init_task;
extern unsigned long init_stack[THREAD_SIZE / sizeof(unsigned long)];

#endif /* !SEMINIX_SCHED_INIT_TASK_H */
