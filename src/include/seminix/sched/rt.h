#ifndef SEMINIX_SCHED_RT_H
#define SEMINIX_SCHED_RT_H

#include <seminix/sched.h>
#include <seminix/tcb.h>

/*
 * default timeslice is 100 msecs (used only for SCHED_RR tasks).
 * Timeslices get refilled after they expire.
 */
#define RR_TIMESLICE		(100 * HZ / 1000)

static inline int rt_policy(int policy)
{
    if (unlikely(policy == SEMINIX_SCHED_FIFO) || unlikely(policy == SEMINIX_SCHED_RR))
        return 1;
    return 0;
}

static inline int rt_prio(int prio)
{
    if (unlikely((prio >= MIN_RT_PRIO) && (prio < MAX_RT_PRIO)))
        return 1;
    return 0;
}

static inline int rt_task(struct tcb *p)
{
    return rt_prio(p->prio);
}

#endif /* !SEMINIX_SCHED_RT_H */
