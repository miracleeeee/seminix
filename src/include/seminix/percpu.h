#ifndef SEMINIX_PERCPU_H
#define SEMINIX_PERCPU_H

#include <asm/percpu.h>

#ifdef CONFIG_SMP
extern void setup_per_cpu_areas(void);
#else
static inline void setup_per_cpu_areas(void) {}
#endif

#endif /* !SEMINIX_PERCPU_H */
