#ifndef SEMINIX_MMDEBUG_H
#define SEMINIX_MMDEBUG_H

struct page;

extern void dump_page(struct page *page, const char *reason);

#endif /* !SEMINIX_MMDEBUG_H */
