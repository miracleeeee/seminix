/* SPDX-License-Identifier: GPL-2.0 */
#ifndef SEMINIX_LINKAGE_H
#define SEMINIX_LINKAGE_H

#include <utils/compiler_types.h>
#include <utils/stringify.h>
#include <utils/pagesize.h>
#include <utils/linkage.h>

#define __page_aligned_data	__section(".data..page_aligned") __aligned(UTILS_PAGE_SIZE)
#define __page_aligned_bss	__section(".bss..page_aligned") __aligned(UTILS_PAGE_SIZE)

/*
 * For assembly routines.
 *
 * Note when using these that you must specify the appropriate
 * alignment directives yourself
 */
#define __PAGE_ALIGNED_DATA	.section ".data..page_aligned", "aw"
#define __PAGE_ALIGNED_BSS	.section ".bss..page_aligned", "aw"

#endif /* !SEMINIX_LINKAGE_H */
