/*
 * sched_clock.h: support for extending counters to full 64-bit ns counter
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#ifndef SEMINIX_SCHED_CLOCK_H
#define SEMINIX_SCHED_CLOCK_H

#include <utils/types.h>

u64 notrace sched_clock(void);

void generic_sched_clock_init(void);

int sched_clock_suspend(void);
void sched_clock_resume(void);

void sched_clock_register(u64 (*read)(void), int bits, u64 rate);

#endif /* !SEMINIX_SCHED_CLOCK_H */
