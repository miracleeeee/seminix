/* SPDX-License-Identifier: GPL-2.0 */
#ifndef SEMINIX_LOCK_QRWLOCK_TYPES_H
#define SEMINIX_LOCK_QRWLOCK_TYPES_H

#include <utils/types.h>
#include <utils/byteorder.h>

/*
 * The queue read/write lock data structure
 */

typedef struct qrwlock {
    union {
        atomic_t cnts;
        struct {
#ifdef ___LITTLE_ENDIAN
            u8 wlocked;	/* Locked for write? */
            u8 __lstate[3];
#else
            u8 __lstate[3];
            u8 wlocked;	/* Locked for write? */
#endif
        };
    };
    arch_spinlock_t		wait_lock;
} arch_rwlock_t;

#define	__ARCH_RW_LOCK_UNLOCKED {		\
    { .cnts = ATOMIC_INIT(0), },		\
    .wait_lock = __ARCH_SPIN_LOCK_UNLOCKED,	\
}

#endif /* !SEMINIX_LOCK_QRWLOCK_TYPES_H */
