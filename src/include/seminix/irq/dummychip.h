#ifndef SEMINIX_IRQ_DUMMYCHIP_H
#define SEMINIX_IRQ_DUMMYCHIP_H

#include <seminix/irq.h>

/* Dummy irq-chip implementations: */
extern struct irq_chip no_irq_chip;

#endif /* !SEMINIX_IRQ_DUMMYCHIP_H */
