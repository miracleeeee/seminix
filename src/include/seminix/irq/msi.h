/* SPDX-License-Identifier: GPL-2.0 */
#ifndef SEMINIX_IRQ_MSI_H
#define SEMINIX_IRQ_MSI_H

#include <utils/types.h>

struct msi_msg {
    u32	address_lo;	/* low 32 bits of msi message address */
    u32	address_hi;	/* high 32 bits of msi message address */
    u32	data;		/* 16 bits of msi message data */
};

struct msi_desc {
    int     irq;
};

#endif /* !SEMINIX_IRQ_MSI_H */
