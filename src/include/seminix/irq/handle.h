#ifndef SEMINIX_IRQ_HANDLE_H
#define SEMINIX_IRQ_HANDLE_H

#include <seminix/cache.h>
#include <seminix/init.h>
#include <seminix/irq.h>

/*
 * Allows interrupt handlers to find the irqchip that's been registered as the
 * top-level IRQ handler.
 */
extern void (*handle_arch_irq)(struct pt_regs *) __ro_after_init;

/*
 * Registers a generic IRQ handling function as the top-level IRQ handler in
 * the system, which is generally the first C code called from an assembly
 * architecture-specific interrupt handler.
 *
 * Returns 0 on success, or -EBUSY if an IRQ handler has already been
 * registered.
 */
int set_handle_irq(void (*handle_irq)(struct pt_regs *));

void handle_bad_irq(struct irq_desc *desc);

irqreturn_t no_action(int cpl, void *dev_id);

irqreturn_t __handle_irq_event_percpu(struct irq_desc *desc, unsigned int *flags);
irqreturn_t handle_irq_event_percpu(struct irq_desc *desc);
irqreturn_t handle_irq_event(struct irq_desc *desc);

void note_interrupt(struct irq_desc *desc, irqreturn_t action_ret);

#endif /* !SEMINIX_IRQ_HANDLE_H */
