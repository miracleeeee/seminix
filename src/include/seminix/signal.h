#ifndef SEMINIX_SIGNAL_H
#define SEMINIX_SIGNAL_H

#include <signal.h>
#include <errno.h>
#include <seminix/thread.h>
#include <seminix/tcb.h>
#include <asm/signal.h>

struct tcb;

#define FPE_FLTUNK 14

/* 特殊的错误号, 仅用于内核处理重启系统调用 */
#define ERESTARTSYS	512
#define ERESTARTNOINTR	513
#define ERESTARTNOHAND	514	/* restart if no handler.. */
#define ERESTART_RESTARTBLOCK 516 /* restart by calling sys_restart_syscall */







static inline int signal_pending(struct tcb *p)
{
    return unlikely(test_tsk_thread_flag(p, TIF_SIGPENDING));
}

static inline int signal_pending_state(long state, struct tcb *p)
{
    if (!(state & TASK_INTERRUPTIBLE))
        return 0;
    if (!signal_pending(p))
        return 0;

    return (state & TASK_INTERRUPTIBLE);
}


static inline sigset_t *sigmask_to_save(void)
{
    return NULL;
}

static inline void restore_saved_sigmask(void)
{
}

struct k_sigaction {
    struct sigaction sa;
};

typedef struct kernel_siginfo {
    siginfo_t siginfo;
} kernel_siginfo_t;

struct ksignal {
    struct k_sigaction ka;
    kernel_siginfo_t info;
    int sig;
};

extern bool get_signal(struct ksignal *ksig);
extern void signal_setup_done(int failed, struct ksignal *ksig, int stepping);

static inline unsigned long sigsp(unsigned long sp, struct ksignal *ksig)
{
    return sp;
}

extern void set_current_blocked(sigset_t *);
int restore_altstack(const stack_t __user *);
int __save_altstack(stack_t __user *, unsigned long);

int copy_siginfo_to_user(siginfo_t __user *to, const kernel_siginfo_t *from);

extern void force_sig(int sig, struct tcb *p);

int force_sig_fault(int sig, int code, void __user *addr, struct tcb *t, const char *reason);
int send_sig_fault(int sig, int code, void __user *addr, struct tcb *t);

#endif /* !SEMINIX_SIGNAL_H */
