/* SPDX-License-Identifier: GPL-2.0 */
#ifndef SEMINIX_OF_RESERVED_MEM_H
#define SEMINIX_OF_RESERVED_MEM_H

#include <utils/types.h>

struct reserved_mem {
    const char			*name;
    unsigned long			fdt_node;
    unsigned long			phandle;
    phys_addr_t			base;
    phys_addr_t			size;
    void				*priv;
};

#ifdef CONFIG_OF_RESERVED_MEM

extern void fdt_reserved_mem_save_node(unsigned long node, const char *uname,
                   phys_addr_t base, phys_addr_t size);

extern struct reserved_mem *of_reserved_mem_lookup(struct device_node *np);

#else

static inline void fdt_reserved_mem_save_node(unsigned long node,
        const char *uname, phys_addr_t base, phys_addr_t size) { }

static inline struct reserved_mem *of_reserved_mem_lookup(struct device_node *np)
{
    return NULL;
}

#endif /* CONFIG_OF_RESERVED_MEM */

#endif /* !SEMINIX_OF_RESERVED_MEM_H */
