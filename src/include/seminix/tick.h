/*  linux/include/linux/tick.h
 *
 *  This file contains the structure definitions for tick related functions
 *
 */
#ifndef SEMINIX_TICK_H
#define SEMINIX_TICK_H

#include <utils/types.h>

struct tcb;

static inline bool tick_nohz_full_cpu(int cpu)
{
    // TODO
    return false;
}

static inline void tick_nohz_idle_enter(void)
{
    // TODO
}

static inline void tick_nohz_idle_exit(void)
{
    // TODO
}

static inline void tick_nohz_task_switch(struct tcb *tsk)
{
    // TODO
}

#endif /* !SEMINIX_TICK_H */
