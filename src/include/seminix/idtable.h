// SPDX-License-Identifier: GPL-2.0
#ifndef SEMINIX_IDTABLE_H
#define SEMINIX_IDTABLE_H

#include <utils/bitops.h>

struct idtable {
    unsigned long *full_bits;
    int max, first, used;
};

int id_table_init(struct idtable *it, int nr, int first);
struct idtable *id_table_create(int nr, int first);
void id_table_destroy(struct idtable *it, bool free_table);
static inline void id_table_uninit(struct idtable *it)
{
    id_table_destroy(it, false);
}

int id_table_get_free(struct idtable *it, int start);
int id_talbe_get_free_noexpand(struct idtable *it, int start);
void id_table_put(struct idtable *it, int nr);

#endif /* !SEMINIX_IDTABLE_H */
