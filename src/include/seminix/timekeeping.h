/* SPDX-License-Identifier: GPL-2.0 */
#ifndef SEMINIX_TIMEKEEPING_H
#define SEMINIX_TIMEKEEPING_H

#include <errno.h>
#include <seminix/ktime.h>

bool timekeeping_is_valid(void);

u32 ktime_get_resolution_ns(void);
u64 timekeeping_max_deferment(void);

ktime_t ktime_get(void);
ktime_t ktime_get_real(void);

time64_t ktime_get_seconds(void);
time64_t ktime_get_real_seconds(void);

void ktime_get_ts64(struct timespec64 *ts);
void ktime_get_real_ts64(struct timespec64 *tv);

ktime_t ktime_get_coarse(void);
ktime_t ktime_get_coarse_real(void);

void ktime_get_coarse_ts64(struct timespec64 *ts);
void ktime_get_coarse_real_ts64(struct timespec64 *ts);

u64 ktime_get_cycles(void);

void read_persistent_clock64(struct timespec64 *ts);
int update_persistent_clock64(struct timespec64 now);

void timekeeping_init(void);
extern void time_init(void);

/*
 * Get and set timeofday
 */
int do_settimeofday64(const struct timespec64 *ts);


static inline u64 ktime_get_ns(void)
{
    return ktime_to_ns(ktime_get());
}

static inline u64 ktime_get_real_ns(void)
{
    return ktime_to_ns(ktime_get_real());
}

/*
 * These interfaces are all based on the old timespec type
 * and should get replaced with the timespec64 based versions
 * over time so we can remove the file here.
 */

static inline time64_t get_seconds(void)
{
    return ktime_get_real_seconds();
}

static inline void getnstimeofday(struct timespec *ts)
{
    struct timespec64 ts64;

    ktime_get_real_ts64(&ts64);
    *ts = timespec64_to_timespec(ts64);
}

static inline void ktime_get_ts(struct timespec *ts)
{
    struct timespec64 ts64;

    ktime_get_ts64(&ts64);
    *ts = timespec64_to_timespec(ts64);
}

static inline void getrawmonotonic(struct timespec *ts)
{
    struct timespec64 ts64;

    ktime_get_ts64(&ts64);
    *ts = timespec64_to_timespec(ts64);
}

#endif /* !SEMINIX_TIMEKEEPING_H */
