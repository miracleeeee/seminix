/* SPDX-License-Identifier: GPL-2.0 */
#ifndef SEMINIX_TIME_H
#define SEMINIX_TIME_H

#include <utils/time.h>

#define MAX_CLOCKS  (CLOCK_MONOTONIC + 1)

/* Located here for timespec[64]_valid_strict */
#define KTIME_MAX           TIME64_MAX
#define KTIME_SEC_MAX       TIME64_SEC_MAX

#ifndef __kernel_timespec
struct __kernel_timespec {
    s64       		  tv_sec;                 /* seconds */
    s64               tv_nsec;                /* nanoseconds */
};
#endif

#ifndef __kernel_itimerspec
struct __kernel_itimerspec {
    struct __kernel_timespec it_interval;    /* timer period */
    struct __kernel_timespec it_value;       /* timer expiration */
};
#endif

int get_timespec64(struct timespec64 *ts,
		const struct __kernel_timespec __user *uts);
int put_timespec64(const struct timespec64 *ts,
		struct __kernel_timespec __user *uts);
int get_itimerspec64(struct itimerspec64 *it,
			const struct __kernel_itimerspec __user *uit);
int put_itimerspec64(const struct itimerspec64 *it,
			struct __kernel_itimerspec __user *uit);

#endif /* !SEMINIX_TIME_H */
