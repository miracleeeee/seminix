/* SPDX-License-Identifier: GPL-2.0 */
#ifndef SEMINIX_STACKPROTECTOR_H
#define SEMINIX_STACKPROTECTOR_H

#ifdef CONFIG_STACKPROTECTOR
#include <asm/stackprotector.h>
extern __visible void __stack_chk_fail(void);
#else
static inline void boot_init_stack_canary(void) {}
#endif /* CONFIG_STACKPROTECTOR */
#endif /* !SEMINIX_STACKPROTECTOR_H */
