#ifndef SEMINIX_VMACACHE_H
#define SEMINIX_VMACACHE_H

#include <seminix/mm.h>
#include <seminix/tcb.h>

static inline void vmacache_flush(struct tcb *tsk)
{
	memset(tsk->vmacache.vmas, 0, sizeof(tsk->vmacache.vmas));
}

void __vmacache_update(struct tcb *tsk, unsigned long addr, struct vm_area_struct *newvma);
static inline void vmacache_update(unsigned long addr, struct vm_area_struct *newvma)
{
	__vmacache_update(NULL, addr, newvma);
}

struct vm_area_struct *__vmacache_find(struct tcb *tsk, struct mm_struct *mm, unsigned long addr);
static inline struct vm_area_struct *vmacache_find(struct mm_struct *mm, unsigned long addr)
{
	return __vmacache_find(NULL, mm, addr);
}

static inline void vmacache_invalidate(struct mm_struct *mm)
{
	mm->vmacache_seqnum++;
}

#endif /* !SEMINIX_VMACACHE_H */
