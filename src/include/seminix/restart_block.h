/* SPDX-License-Identifier: GPL-2.0 */
/*
 * Common syscall restarting data
 */
#ifndef SEMINIX_RESTART_BLOCK_H
#define SEMINIX_RESTART_BLOCK_H

/*
 * System call restart block.
 */
struct restart_block {
    long (*fn)(struct restart_block *);
};

extern long do_no_restart_syscall(struct restart_block *parm);

#endif /* !SEMINIX_RESTART_BLOCK_H */
