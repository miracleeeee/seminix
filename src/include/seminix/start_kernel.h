#ifndef SEMINIX_START_KERNEL_H
#define SEMINIX_START_KERNEL_H

#include <utils/types.h>
#include <seminix/linkage.h>

struct tcb;

extern phys_addr_t phys_initrd_start;
extern unsigned long phys_initrd_size;

/*
 * Values used for system_state. Ordering of the states must not be changed
 * as code checks for <, <=, >, >= STATE.
 */
extern enum system_states {
    SYSTEM_BOOTING,
    SYSTEM_RUNNING,
    SYSTEM_HALT,
    SYSTEM_POWER_OFF,
    SYSTEM_RESTART,
    SYSTEM_SUSPEND,
} system_state;

#define COMMAND_LINE_SIZE	2048
extern char boot_command_line[COMMAND_LINE_SIZE];

asmlinkage __visible void start_kernel(void);

extern void setup_arch(void);
extern struct tcb *rootserver_init(void);

#endif /* !SEMINIX_START_KERNEL_H */
