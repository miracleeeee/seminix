// SPDX-License-Identifier: GPL-2.0
#include <seminix/pgtable.h>
#include <seminix/mm_types.h>
#include <asm/mmu.h>

#ifndef INIT_MM_CONTEXT
#define INIT_MM_CONTEXT(name)
#endif

/*
 * For dynamically allocated mm_structs, there is a dynamically sized cpumask
 * at the end of the structure, the size of which depends on the maximum CPU
 * number the system can see. That way we allocate only as much memory for
 * mm_cpumask() as needed for the hundreds, or thousands of processes that
 * a system typically runs.
 *
 * Since there is only one init_mm in the entire system, keep it simple
 * and size this cpu_bitmask to NR_CPUS.
 */
struct mm_struct init_mm = {
    .mm_rb      = RB_ROOT,
    .pgd		= swapper_pg_dir,
    .mm_users   = ATOMIC_INIT(1),
    .page_table_lock = __SPIN_LOCK_UNLOCKED(init_mm.page_table_lock),
    .mmap_sem   = __RWSEM_INITIALIZER(init_mm.mmap_sem),
    .tl_lock = __SPIN_LOCK_UNLOCKED(init_mm.tl_lock),
    .thread_list = LIST_HEAD_INIT(init_mm.thread_list),
    INIT_MM_CONTEXT(init_mm)
};
