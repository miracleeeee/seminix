// SPDX-License-Identifier: GPL-2.0
/*
 * mm/debug.c
 *
 * mm/ specific debug routines.
 *
 */
#include <seminix/mm.h>

void dump_page(struct page *page, const char *reason)
{
    bool page_poisoned = PagePoisoned(page);
    int mapcount;

    /*
     * If struct page is poisoned don't access Page*() functions as that
     * leads to recursive loop. Page*() check for poisoned pages, and calls
     * dump_page() when detected.
     */
    if (page_poisoned) {
        pr_warn("page:%px is uninitialized and poisoned", page);
        goto hex_only;
    }

    /*
     * Avoid VM_BUG_ON() in page_mapcount().
     * page->_mapcount space in struct page is used by sl[aou]b pages to
     * encode own info.
     */
    mapcount = PageSlab(page) ? 0 : page_mapcount(page);

    pr_warn("page:%px refcount:%d mapcount:%d",
          page, page_ref_count(page), mapcount);
    pr_cont("\n");
    pr_warn("flags: %#lx(%pGp)\n", page->flags, &page->flags);
hex_only:
    print_hex_dump(KERN_WARNING, "raw: ", DUMP_PREFIX_NONE, 32,
            sizeof(unsigned long), page, sizeof(struct page),
            false, printk);

    if (reason)
        pr_warn("page dumped because: %s\n", reason);
}
