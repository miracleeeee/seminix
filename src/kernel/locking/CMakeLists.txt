# SPDX-License-Identifier: Apache-2.0

if(CONFIG_SMP)
  kernel_library()
  kernel_library_sources(
    qspinlock.c qrwlock.c spinlock.c
    mutex.c rwsem.c semaphore.c
  )
endif()
