// SPDX-License-Identifier: GPL-2.0

#include <errno.h>
#include <seminix/kernel.h>
#include <seminix/linkage.h>
#include <seminix/syscall.h>
#include <seminix/dump_stack.h>
#include <asm/ptrace.h>

asmlinkage long sys_ni_syscall(void);

/*
 * Non-implemented system calls get redirected here.
 */
asmlinkage long sys_ni_syscall(void)
{
    // TODO 调试使用
    panic("%s sys: %d\n", __func__, syscall_get_nr(current, current_pt_regs()));

    return -ENOSYS;
}
