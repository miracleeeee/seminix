/*
 * idle-task scheduling class.
 *
 * (NOTE: these are not related to SCHED_IDLE tasks which are
 *  handled in sched_fair.c)
 */
#include "sched.h"

/*
 * Idle tasks are unconditionally rescheduled:
 */
static void check_preempt_curr_idle(struct rq *rq, struct tcb *p, int flags)
{
    resched_task(rq->idle);
}

static struct tcb *pick_next_task_idle(struct rq *rq)
{
    return rq->idle;
}

/*
 * It is not legal to sleep in the idle task - print a warning
 * message if some code attempts to do it:
 */
static void
dequeue_task_idle(struct rq *rq, struct tcb *p, int flags)
{
    spin_unlock_irq(&rq->lock);
    printk(KERN_ERR "bad: scheduling from the idle thread!\n");
    dump_stack();
    spin_lock_irq(&rq->lock);
}

static void put_prev_task_idle(struct rq *rq, struct tcb *prev)
{
}

static void task_tick_idle(struct rq *rq, struct tcb *curr, int queued)
{
}

static void set_curr_task_idle(struct rq *rq)
{
}

static void switched_to_idle(struct rq *rq, struct tcb *p)
{
    BUG();
}

static void
prio_changed_idle(struct rq *rq, struct tcb *p, int oldprio)
{
    BUG();
}

static unsigned int get_rr_interval_idle(struct rq *rq, struct tcb *task)
{
    return 0;
}

/*
 * Simple, special scheduling class for the per-CPU idle tasks:
 */
const struct sched_class idle_sched_class = {
    .next               = NULL,
    /* no enqueue/yield_task for idle tasks */

    /* dequeue is not valid, we print a debug message there: */
    .dequeue_task		= dequeue_task_idle,

    .check_preempt_curr	= check_preempt_curr_idle,

    .pick_next_task	    = pick_next_task_idle,
    .put_prev_task	    = put_prev_task_idle,

    .set_curr_task      = set_curr_task_idle,
    .task_tick          = task_tick_idle,

    .get_rr_interval    = get_rr_interval_idle,

    .prio_changed       = prio_changed_idle,
    .switched_to        = switched_to_idle,
};
