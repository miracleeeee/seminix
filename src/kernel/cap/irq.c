#include <seminix/syscall.h>
#include <cap/cap.h>
#include <cap/irq.h>

SYSCALL_DEFINE4(irq_control_get, int, irq_control, int, cnode, int *, irq,
    unsigned long *, params)
{
    return 0;
}

SYSCALL_DEFINE1(irq_handler_ack, int, irq_cd)
{
    return 0;
}

SYSCALL_DEFINE2(irq_set_handler_notification, int, irq_cd, int, notification)
{
    return 0;
}

SYSCALL_DEFINE1(irq_clear_handler_notification, int, irq_cd)
{
    return 0;
}
