# SPDX-License-Identifier: Apache-2.0

kernel_library()
kernel_library_sources(
  cap.c cnode.c endpoint.c frame.c rlimit.c tcb.c
  irq.c vspace.c signal.c
)
