#include <seminix/syscall.h>
#include <seminix/kernel.h>
#include <seminix/sysctl.h>
#include <seminix/cache.h>
#include <seminix/uaccess.h>

#define MAPCOUNT_ELF_CORE_MARGIN	(5)
#define DEFAULT_MAX_MAP_COUNT	(USHRT_MAX - MAPCOUNT_ELF_CORE_MARGIN)

int sysctl_max_map_count __read_mostly = DEFAULT_MAX_MAP_COUNT;

static int do_sysctl_get(sysctl_t __user *sysctl)
{
    sysctl_t sys;

    if (copy_from_user(&sys, sysctl, sizeof (*sysctl)))
        return -EFAULT;

    switch (sys.type) {
    case SYSCTL_TYPE_IPCBUFFER:
        if (!current->ipc_buffer)
            return -SERRNO_EINVAL;
        sys.sysctl.ipcbuf.uaddr = current->user_ipc_buffer;
        if (copy_to_user(sysctl, &sys, sizeof (*sysctl)))
            return -EFAULT;
        break;
    default:
        panic("Not impl get\n");
    }
    return 0;
}

SYSCALL_DEFINE2(sysctl, int, op, sysctl_t __user *, sysctl)
{
    switch (op) {
    case SYSCTL_CMD_GET:
        return do_sysctl_get(sysctl);
    case SYSCTL_CMD_SET:
        panic("Sysctl: Not impl: op %d\n", op);
    }
    return 0;
}
