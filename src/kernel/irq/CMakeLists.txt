# SPDX-License-Identifier: Apache-2.0

kernel_library()
kernel_library_sources(
  handle.c irqdomain.c irqdesc.c dummychip.c
  manage.c irq_regs.c chip.c
)
