/* SPDX-License-Identifier: GPL-2.0 */
/*
 * Debugging printout:
 */
#ifndef KERNEL_IRQ_INTERIANL_DEBUG_H
#define KERNEL_IRQ_INTERIANL_DEBUG_H

#define ___P(f) if (desc->status_use_accessors & f) printk("%14s set\n", #f)
#define ___PP(cpu, f) if (desc->status_percpu[cpu] & f) printk("%14s set on cpu%d\n", #f, cpu)

static inline void print_irq_desc(int irq, struct irq_desc *desc)
{
    int cpu;

	printk("irq %d, desc: %p, depth: %d, count: %d, unhandled: %d\n",
		irq, desc, desc->depth, desc->irq_count, desc->irqs_unhandled);
	printk("->handle_irq():  %p\n", desc->handle_irq);
	printk("->irq_data.chip(): %p\n", desc->irq_data.chip);
	printk("->action(): %p\n", desc->action);
	if (desc->action)
		printk("->action->handler(): %p\n", desc->action->handler);

	___P(IRQ_PER_CPU);

    if (desc->status_use_accessors & IRQ_PER_CPU) {
        BUG_ON(!desc->status_percpu);

        for_each_possible_cpu(cpu) {
            ___PP(cpu, IRQ_LEVEL);

            ___PP(cpu, IRQ_NOREQUEST);
            ___PP(cpu, IRQ_NO_BALANCING);

            ___PP(cpu, IRQ_WAITING);
            ___PP(cpu, IRQ_PENDING);
            ___PP(cpu, IRQ_INIPC);

            ___PP(cpu, IRQ_MASKED);
            ___PP(cpu, IRQ_INPROGRESS);
            ___PP(cpu, IRQ_STARTED);
        }
    } else {
        ___P(IRQ_LEVEL);

        ___P(IRQ_NOREQUEST);
        ___P(IRQ_NO_BALANCING);

        ___P(IRQ_WAITING);
        ___P(IRQ_PENDING);
        ___P(IRQ_INIPC);

        ___P(IRQ_MASKED);
        ___P(IRQ_INPROGRESS);
        ___P(IRQ_STARTED);
    }
}

#undef ___P
#undef ___PP

#endif /* !KERNEL_IRQ_INTERIANL_DEBUG_H */
