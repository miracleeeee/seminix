#ifndef KERNLE_IRQ_INTERNAL_H
#define KERNLE_IRQ_INTERNAL_H

#include "debug.h"

extern struct irqaction chained_action;

static inline int irq_desc_is_chained(struct irq_desc *desc)
{
    return (desc->action && desc->action == &chained_action);
}

#endif /* !KERNLE_IRQ_INTERNAL_H */
