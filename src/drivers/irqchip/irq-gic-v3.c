/*
 *  Copyright (C) 2002 ARM Limited, All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Interrupt architecture for the GIC:
 *
 * o There is one Interrupt Distributor, which receives interrupts
 *   from system devices and sends them to the Interrupt Controllers.
 *
 * o There is one CPU Interface per CPU, which sends interrupts sent
 *   by the Distributor, and interrupts generated locally, to the
 *   associated CPU. The base address of the CPU interface is usually
 *   aliased so that the same address points to different chips depending
 *   on the CPU it is accessed from.
 *
 * Note that IRQs 0-31 are special - they are local to each CPU.
 * As such, the enable set/clear, pending set/clear and active bit
 * registers are banked per-cpu for these sources.
 */
#define pr_fmt(fmt)	"GICv3: " fmt

#include <utils/err.h>
#include <seminix/init.h>
#include <seminix/of.h>
#include <seminix/of_irq.h>
#include <seminix/of_address.h>
#include <seminix/cache.h>
#include <seminix/smp.h>
#include <seminix/cpu.h>
#include <seminix/slab.h>
#include <devices/irqchip.h>
#include <devices/irqchip/arm-gic-common.h>
#include <devices/irqchip/arm-gic-v3.h>
#include <seminix/irq.h>
#include <seminix/irqdesc.h>
#include <seminix/irqdomain.h>
#include <seminix/irq/chip.h>
#include <seminix/irq/handle.h>
#include <seminix/irq/manage.h>
#include <seminix/delay.h>
#include <asm/io.h>
#include <asm/traps.h>
#include <asm/boot.h>
#include "irq-gic-common.h"

#define FLAGS_WORKAROUND_GICR_WAKER_MSM8996	(1ULL << 0)

#define HWIRQ_MAX   2048

struct redist_region {
    void __iomem		*redist_base;
    phys_addr_t		phys_base;
    bool			single_redist;
};

struct gic_chip_data {
    struct fwnode_handle	*fwnode;
    void __iomem		*dist_base;
    struct redist_region	*redist_regions;
    struct rdists		rdists;
    struct irq_domain	*domain;
    u64			redist_stride;
    u32			nr_redist_regions;
    u64			flags;
    bool			has_rss;
    unsigned int		irq_nr;
};

static struct gic_chip_data gic_data __read_mostly;
static bool supports_deactivate_key = true;

static struct gic_kvm_info gic_v3_kvm_info;
static DEFINE_PER_CPU(bool, has_rss);
static DEFINE_PER_CPU(struct rdist, rdist_percpu);

#define MPIDR_RS(mpidr)			(((mpidr) & 0xF0UL) >> 4)
#define gic_data_rdist()		(this_cpu_ptr(gic_data.rdists.rdist))
#define gic_data_rdist_rd_base()	(gic_data_rdist()->rd_base)
#define gic_data_rdist_sgi_base()	(gic_data_rdist_rd_base() + SZ_64K)

/* Our default, arbitrary priority value. Linux only uses one anyway. */
#define DEFAULT_PMR_VALUE	0xf0

static inline unsigned int gic_irq(struct irq_data *d)
{
    return d->hwirq;
}

static inline int gic_irq_in_rdist(struct irq_data *d)
{
    return gic_irq(d) < 32;
}

static inline void __iomem *gic_dist_base(struct irq_data *d)
{
    if (gic_irq_in_rdist(d))	/* SGI+PPI -> SGI_base for this CPU */
        return gic_data_rdist_sgi_base();

    if (d->hwirq <= 1023)		/* SPI -> dist_base */
        return gic_data.dist_base;

    return NULL;
}

static void gic_do_wait_for_rwp(void __iomem *base)
{
    u32 count = 1000000;	/* 1s! */

    while (readl_relaxed(base + GICD_CTLR) & GICD_CTLR_RWP) {
        count--;
        if (!count) {
            pr_err("RWP timeout, gone fishing\n");
            return;
        }
        cpu_relax();
        udelay(1);
    };
}

/* Wait for completion of a distributor change */
static void gic_dist_wait_for_rwp(void)
{
    gic_do_wait_for_rwp(gic_data.dist_base);
}

/* Wait for completion of a redistributor change */
static void gic_redist_wait_for_rwp(void)
{
    gic_do_wait_for_rwp(gic_data_rdist_rd_base());
}

#ifdef CONFIG_ARM64
static u64 __maybe_unused gic_read_iar(void)
{
    return gic_read_iar_common();
}
#endif

static void gic_enable_redist(bool enable)
{
    void __iomem *rbase;
    u32 count = 1000000;	/* 1s! */
    u32 val;

    if (gic_data.flags & FLAGS_WORKAROUND_GICR_WAKER_MSM8996)
        return;

    rbase = gic_data_rdist_rd_base();

    val = readl_relaxed(rbase + GICR_WAKER);
    if (enable)
        /* Wake up this CPU redistributor */
        val &= ~GICR_WAKER_ProcessorSleep;
    else
        val |= GICR_WAKER_ProcessorSleep;
    writel_relaxed(val, rbase + GICR_WAKER);

    if (!enable) {		/* Check that GICR_WAKER is writeable */
        val = readl_relaxed(rbase + GICR_WAKER);
        if (!(val & GICR_WAKER_ProcessorSleep))
            return;	/* No PM support in this redistributor */
    }

    while (--count) {
        val = readl_relaxed(rbase + GICR_WAKER);
        if (enable ^ (bool)(val & GICR_WAKER_ChildrenAsleep))
            break;
        cpu_relax();
        udelay(1);
    };
    if (!count)
        pr_err("redistributor failed to %s...\n",
                   enable ? "wakeup" : "sleep");
}

/*
 * Routines to disable, enable, EOI and route interrupts
 */
static int gic_peek_irq(struct irq_data *d, u32 offset)
{
    u32 mask = 1 << (gic_irq(d) % 32);
    void __iomem *base;

    if (gic_irq_in_rdist(d))
        base = gic_data_rdist_sgi_base();
    else
        base = gic_data.dist_base;

    return !!(readl_relaxed(base + offset + (gic_irq(d) / 32) * 4) & mask);
}

static void gic_poke_irq(struct irq_data *d, u32 offset)
{
    u32 mask = 1 << (gic_irq(d) % 32);
    void (*rwp_wait)(void);
    void __iomem *base;

    if (gic_irq_in_rdist(d)) {
        base = gic_data_rdist_sgi_base();
        rwp_wait = gic_redist_wait_for_rwp;
    } else {
        base = gic_data.dist_base;
        rwp_wait = gic_dist_wait_for_rwp;
    }

    writel_relaxed(mask, base + offset + (gic_irq(d) / 32) * 4);
    rwp_wait();
}

static void gic_mask_irq(struct irq_data *d)
{
    gic_poke_irq(d, GICD_ICENABLER);
}

static void gic_eoimode1_mask_irq(struct irq_data *d)
{
    gic_mask_irq(d);
}

static void gic_unmask_irq(struct irq_data *d)
{
    gic_poke_irq(d, GICD_ISENABLER);
}

static int gic_irq_set_irqchip_state(struct irq_data *d,
                     enum irqchip_irq_state which, bool val)
{
    u32 reg;

    if (d->hwirq >= (int)gic_data.irq_nr) /* PPI/SPI only */
        return -EINVAL;

    switch (which) {
    case IRQCHIP_STATE_PENDING:
        reg = val ? GICD_ISPENDR : GICD_ICPENDR;
        break;

    case IRQCHIP_STATE_ACTIVE:
        reg = val ? GICD_ISACTIVER : GICD_ICACTIVER;
        break;

    case IRQCHIP_STATE_MASKED:
        reg = val ? GICD_ICENABLER : GICD_ISENABLER;
        break;

    default:
        return -EINVAL;
    }

    gic_poke_irq(d, reg);
    return 0;
}

static int gic_irq_get_irqchip_state(struct irq_data *d,
                     enum irqchip_irq_state which, bool *val)
{
    if (d->hwirq >= (int)gic_data.irq_nr) /* PPI/SPI only */
        return -EINVAL;

    switch (which) {
    case IRQCHIP_STATE_PENDING:
        *val = gic_peek_irq(d, GICD_ISPENDR);
        break;

    case IRQCHIP_STATE_ACTIVE:
        *val = gic_peek_irq(d, GICD_ISACTIVER);
        break;

    case IRQCHIP_STATE_MASKED:
        *val = !gic_peek_irq(d, GICD_ISENABLER);
        break;

    default:
        return -EINVAL;
    }

    return 0;
}

static void gic_eoi_irq(struct irq_data *d)
{
    gic_write_eoir(gic_irq(d));
}

static void gic_eoimode1_eoi_irq(struct irq_data *d)
{
    /*
     * No need to deactivate an LPI, or an interrupt that
     * is is getting forwarded to a vcpu.
     */
    if (gic_irq(d) >= 8192)
        return;
    gic_write_dir(gic_irq(d));
}

static int gic_set_type(struct irq_data *d, unsigned int type)
{
    int irq = gic_irq(d);
    void (*rwp_wait)(void);
    void __iomem *base;

    /* Interrupt configuration for SGIs can't be changed */
    if (irq < 16)
        return -EINVAL;

    /* SPIs have restrictions on the supported types */
    if (irq >= 32 && type != IRQ_TYPE_LEVEL_HIGH &&
             type != IRQ_TYPE_EDGE_RISING)
        return -EINVAL;

    if (gic_irq_in_rdist(d)) {
        base = gic_data_rdist_sgi_base();
        rwp_wait = gic_redist_wait_for_rwp;
    } else {
        base = gic_data.dist_base;
        rwp_wait = gic_dist_wait_for_rwp;
    }

    return gic_configure_irq(irq, type, base, rwp_wait);
}

static u64 gic_mpidr_to_affinity(unsigned long mpidr)
{
    u64 aff;

    aff = ((u64)MPIDR_AFFINITY_LEVEL(mpidr, 3) << 32 |
           MPIDR_AFFINITY_LEVEL(mpidr, 2) << 16 |
           MPIDR_AFFINITY_LEVEL(mpidr, 1) << 8  |
           MPIDR_AFFINITY_LEVEL(mpidr, 0));

    return aff;
}

static asmlinkage void __irq_entry gic_handle_irq(struct pt_regs *regs)
{
    u32 irqnr;

    irqnr = gic_read_iar();

    if (likely(irqnr > 15 && irqnr < 1020) || irqnr >= 8192) {
        int err;

        if (likely(supports_deactivate_key))
            gic_write_eoir(irqnr);
        else
            isb();

        err = handle_domain_irq(gic_data.domain, irqnr, regs);
        if (err) {
            WARN_ONCE(true, "Unexpected interrupt received!\n");
            if (likely(supports_deactivate_key)) {
                if (irqnr < 8192)
                    gic_write_dir(irqnr);
            } else {
                gic_write_eoir(irqnr);
            }
        }
        return;
    }
    if (irqnr < 16) {
        gic_write_eoir(irqnr);
        if (likely(supports_deactivate_key))
            gic_write_dir(irqnr);
        /*
         * Unlike GICv2, we don't need an smp_rmb() here.
         * The control dependency from gic_read_iar to
         * the ISB in gic_write_eoir is enough to ensure
         * that any shared data read by handle_IPI will
         * be read after the ACK.
         */
        handle_IPI(irqnr, regs);
    }
}

static void __init gic_dist_init(void)
{
    unsigned int i;
    u64 affinity;
    void __iomem *base = gic_data.dist_base;

    /* Disable the distributor */
    writel_relaxed(0, base + GICD_CTLR);
    gic_dist_wait_for_rwp();

    /*
     * Configure SPIs as non-secure Group-1. This will only matter
     * if the GIC only has a single security state. This will not
     * do the right thing if the kernel is running in secure mode,
     * but that's not the intended use case anyway.
     */
    for (i = 32; i < gic_data.irq_nr; i += 32)
        writel_relaxed(~0, base + GICD_IGROUPR + i / 8);

    gic_dist_config(base, gic_data.irq_nr, gic_dist_wait_for_rwp);

    /* Enable distributor with ARE, Group1 */
    writel_relaxed(GICD_CTLR_ARE_NS | GICD_CTLR_ENABLE_G1A | GICD_CTLR_ENABLE_G1,
               base + GICD_CTLR);

    /*
     * Set all global interrupts to the boot CPU only. ARE must be
     * enabled.
     */
    affinity = gic_mpidr_to_affinity(cpu_logical_map(smp_processor_id()));
    for (i = 32; i < gic_data.irq_nr; i++)
        gic_write_irouter(affinity, base + GICD_IROUTER + i * 8);
}

static int gic_iterate_rdists(int (*fn)(struct redist_region *, void __iomem *))
{
    int ret = -ENODEV;
    int i;

    for (i = 0; i < (int)gic_data.nr_redist_regions; i++) {
        void __iomem *ptr = gic_data.redist_regions[i].redist_base;
        u64 typer;
        u32 reg;

        reg = readl_relaxed(ptr + GICR_PIDR2) & GIC_PIDR2_ARCH_MASK;
        if (reg != GIC_PIDR2_ARCH_GICv3 &&
            reg != GIC_PIDR2_ARCH_GICv4) { /* We're in trouble... */
            pr_warn("No redistributor present @%p\n", ptr);
            break;
        }

        do {
            typer = gic_read_typer(ptr + GICR_TYPER);
            ret = fn(gic_data.redist_regions + i, ptr);
            if (!ret)
                return 0;

            if (gic_data.redist_regions[i].single_redist)
                break;

            if (gic_data.redist_stride) {
                ptr += gic_data.redist_stride;
            } else {
                ptr += SZ_64K * 2; /* Skip RD_base + SGI_base */
                if (typer & GICR_TYPER_VLPIS)
                    ptr += SZ_64K * 2; /* Skip VLPI_base + reserved page */
            }
        } while (!(typer & GICR_TYPER_LAST));
    }

    return ret ? -ENODEV : 0;
}

static int __gic_populate_rdist(struct redist_region *region, void __iomem *ptr)
{
    unsigned long mpidr = cpu_logical_map(smp_processor_id());
    u64 typer;
    u32 aff;

    /*
     * Convert affinity to a 32bit value that can be matched to
     * GICR_TYPER bits [63:32].
     */
    aff = (MPIDR_AFFINITY_LEVEL(mpidr, 3) << 24 |
           MPIDR_AFFINITY_LEVEL(mpidr, 2) << 16 |
           MPIDR_AFFINITY_LEVEL(mpidr, 1) << 8 |
           MPIDR_AFFINITY_LEVEL(mpidr, 0));

    typer = gic_read_typer(ptr + GICR_TYPER);
    if ((typer >> 32) == aff) {
        u64 offset = ptr - region->redist_base;
        gic_data_rdist_rd_base() = ptr;
        gic_data_rdist()->phys_base = region->phys_base + offset;

        pr_info("CPU%d: found redistributor %lx region %d:%pa\n",
            smp_processor_id(), mpidr,
            (int)(region - gic_data.redist_regions),
            &gic_data_rdist()->phys_base);
        return 0;
    }

    /* Try next one */
    return 1;
}

static int gic_populate_rdist(void)
{
    if (gic_iterate_rdists(__gic_populate_rdist) == 0)
        return 0;

    /* We couldn't even deal with ourselves... */
    WARN(true, "CPU%d: mpidr %lx has no re-distributor!\n",
         smp_processor_id(),
         (unsigned long)cpu_logical_map(smp_processor_id()));
    return -ENODEV;
}

static int __gic_update_vlpi_properties(struct redist_region *region,
                    void __iomem *ptr)
{
    u64 typer = gic_read_typer(ptr + GICR_TYPER);
    gic_data.rdists.has_vlpis &= !!(typer & GICR_TYPER_VLPIS);
    gic_data.rdists.has_direct_lpi &= !!(typer & GICR_TYPER_DirectLPIS);

    return 1;
}

static void gic_update_vlpi_properties(void)
{
    gic_iterate_rdists(__gic_update_vlpi_properties);
    pr_info("%sVLPI support, %sdirect LPI support\n",
        !gic_data.rdists.has_vlpis ? "no " : "",
        !gic_data.rdists.has_direct_lpi ? "no " : "");
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wimplicit-fallthrough"

static void gic_cpu_sys_reg_init(void)
{
    int i, cpu = smp_processor_id();
    u64 mpidr = cpu_logical_map(cpu);
    u64 need_rss = MPIDR_RS(mpidr);
    bool group0;
    u32 val, pribits;

    /*
     * Need to check that the SRE bit has actually been set. If
     * not, it means that SRE is disabled at EL2. We're going to
     * die painfully, and there is nothing we can do about it.
     *
     * Kindly inform the luser.
     */
    if (!gic_enable_sre())
        pr_err("GIC: unable to set SRE (disabled at EL2), panic ahead\n");

    pribits = gic_read_ctlr();
    pribits &= ICC_CTLR_EL1_PRI_BITS_MASK;
    pribits >>= ICC_CTLR_EL1_PRI_BITS_SHIFT;
    pribits++;

    /*
     * Let's find out if Group0 is under control of EL3 or not by
     * setting the highest possible, non-zero priority in PMR.
     *
     * If SCR_EL3.FIQ is set, the priority gets shifted down in
     * order for the CPU interface to set bit 7, and keep the
     * actual priority in the non-secure range. In the process, it
     * looses the least significant bit and the actual priority
     * becomes 0x80. Reading it back returns 0, indicating that
     * we're don't have access to Group0.
     */
    write_gicreg(BIT(8 - pribits), ICC_PMR_EL1);
    val = read_gicreg(ICC_PMR_EL1);
    group0 = val != 0;

    /* Set priority mask register */
    write_gicreg(DEFAULT_PMR_VALUE, ICC_PMR_EL1);

    /*
     * Some firmwares hand over to the kernel with the BPR changed from
     * its reset value (and with a value large enough to prevent
     * any pre-emptive interrupts from working at all). Writing a zero
     * to BPR restores is reset value.
     */
    gic_write_bpr1(0);

    if (likely(supports_deactivate_key)) {
        /* EOI drops priority only (mode 1) */
        gic_write_ctlr(ICC_CTLR_EL1_EOImode_drop);
    } else {
        /* EOI deactivates interrupt too (mode 0) */
        gic_write_ctlr(ICC_CTLR_EL1_EOImode_drop_dir);
    }

    /* Always whack Group0 before Group1 */
    if (group0) {
        switch(pribits) {
        case 8:
        case 7:
            write_gicreg(0, ICC_AP0R3_EL1);
            write_gicreg(0, ICC_AP0R2_EL1);
        case 6:
            write_gicreg(0, ICC_AP0R1_EL1);
        case 5:
        case 4:
            write_gicreg(0, ICC_AP0R0_EL1);
        }

        isb();
    }

    switch(pribits) {
    case 8:
    case 7:
        write_gicreg(0, ICC_AP1R3_EL1);
        write_gicreg(0, ICC_AP1R2_EL1);
    case 6:
        write_gicreg(0, ICC_AP1R1_EL1);
    case 5:
    case 4:
        write_gicreg(0, ICC_AP1R0_EL1);
    }

    isb();

    /* ... and let's hit the road... */
    gic_write_grpen1(1);

    /* Keep the RSS capability status in per_cpu variable */
    per_cpu(has_rss, cpu) = !!(gic_read_ctlr() & ICC_CTLR_EL1_RSS);

    /* Check all the CPUs have capable of sending SGIs to other CPUs */
    for_each_online_cpu(i) {
        bool have_rss = per_cpu(has_rss, i) && per_cpu(has_rss, cpu);

        need_rss |= MPIDR_RS(cpu_logical_map(i));
        if (need_rss && (!have_rss))
            pr_crit("CPU%d (%lx) can't SGI CPU%d (%lx), no RSS\n",
                cpu, (unsigned long)mpidr,
                i, (unsigned long)cpu_logical_map(i));
    }

    /**
     * GIC spec says, when ICC_CTLR_EL1.RSS==1 and GICD_TYPER.RSS==0,
     * writing ICC_ASGI1R_EL1 register with RS != 0 is a CONSTRAINED
     * UNPREDICTABLE choice of :
     *   - The write is ignored.
     *   - The RS field is treated as 0.
     */
    if (need_rss && (!gic_data.has_rss))
        pr_crit("RSS is required but GICD doesn't support it\n");
}

#pragma GCC diagnostic pop

static int gic_dist_supports_lpis(void)
{
    return false;
}

static void gic_cpu_init(void)
{
    void __iomem *rbase;

    /* Register ourselves with the rest of the world */
    if (gic_populate_rdist())
        return;

    gic_enable_redist(true);

    rbase = gic_data_rdist_sgi_base();

    /* Configure SGIs/PPIs as non-secure Group-1 */
    writel_relaxed(~0, rbase + GICR_IGROUPR0);

    gic_cpu_config(rbase, gic_redist_wait_for_rwp);

    /* initialise system registers */
    gic_cpu_sys_reg_init();
}

#define MPIDR_TO_SGI_RS(mpidr)	(MPIDR_RS(mpidr) << ICC_SGI1R_RS_SHIFT)
#define MPIDR_TO_SGI_CLUSTER_ID(mpidr)	((mpidr) & ~0xFUL)

static int gic_starting_cpu(int cpu)
{
    gic_cpu_init();

    if (gic_dist_supports_lpis())
        pr_warn("Current not supprot lpis\n");

    return 0;
}

static u16 gic_compute_target_list(int *base_cpu, const struct cpumask *mask,
                   unsigned long cluster_id)
{
    int next_cpu, cpu = *base_cpu;
    unsigned long mpidr = cpu_logical_map(cpu);
    u16 tlist = 0;

    while (cpu < nr_cpu_ids) {
        tlist |= 1 << (mpidr & 0xf);

        next_cpu = cpumask_next(cpu, mask);
        if (next_cpu >= nr_cpu_ids)
            goto out;
        cpu = next_cpu;

        mpidr = cpu_logical_map(cpu);

        if (cluster_id != MPIDR_TO_SGI_CLUSTER_ID(mpidr)) {
            cpu--;
            goto out;
        }
    }
out:
    *base_cpu = cpu;
    return tlist;
}

#define MPIDR_TO_SGI_AFFINITY(cluster_id, level) \
    (MPIDR_AFFINITY_LEVEL(cluster_id, level) \
        << ICC_SGI1R_AFFINITY_## level ##_SHIFT)

static void gic_send_sgi(u64 cluster_id, u16 tlist, int irq)
{
    u64 val;

    val = (MPIDR_TO_SGI_AFFINITY(cluster_id, 3)	|
           MPIDR_TO_SGI_AFFINITY(cluster_id, 2)	|
           irq << ICC_SGI1R_SGI_ID_SHIFT		|
           MPIDR_TO_SGI_AFFINITY(cluster_id, 1)	|
           MPIDR_TO_SGI_RS(cluster_id)		|
           tlist << ICC_SGI1R_TARGET_LIST_SHIFT);

    pr_debug("CPU%d: ICC_SGI1R_EL1 %llx\n", smp_processor_id(), val);
    gic_write_sgi1r(val);
}

static void gic_raise_softirq(const struct cpumask *mask, int irq)
{
    int cpu;

    if (WARN_ON(irq >= 16))
        return;

    /*
     * Ensure that stores to Normal memory are visible to the
     * other CPUs before issuing the IPI.
     */
    wmb();

    for_each_cpu(cpu, mask) {
        u64 cluster_id = MPIDR_TO_SGI_CLUSTER_ID(cpu_logical_map(cpu));
        u16 tlist;

        tlist = gic_compute_target_list(&cpu, mask, cluster_id);
        gic_send_sgi(cluster_id, tlist, irq);
    }

    /* Force the above writes to ICC_SGI1R_EL1 to be executed */
    isb();
}

static void gic_smp_init(void)
{
    int ret;

    set_smp_cross_call(gic_raise_softirq);
    ret = cpuhp_register_callback(gic_starting_cpu, "irqchip/arm/gicv3:starting", CPUHP_IRQ_GIC_STARTING);
    if (ret)
        pr_err("Gicv3 starting cpu failed to regiser cpuhp!\n");
}

static int gic_set_affinity(struct irq_data *d, const struct cpumask *mask_val,
                bool force)
{
    int cpu;
    void __iomem *reg;
    int enabled;
    u64 val;

    if (force)
        cpu = cpumask_first(mask_val);
    else
        cpu = cpumask_any_and(mask_val, cpu_online_mask);

    if (cpu >= nr_cpu_ids)
        return -EINVAL;

    if (gic_irq_in_rdist(d))
        return -EINVAL;

    /* If interrupt was enabled, disable it first */
    enabled = gic_peek_irq(d, GICD_ISENABLER);
    if (enabled)
        gic_mask_irq(d);

    reg = gic_dist_base(d) + GICD_IROUTER + (gic_irq(d) * 8);
    val = gic_mpidr_to_affinity(cpu_logical_map(cpu));

    gic_write_irouter(val, reg);

    /*
     * If the interrupt was enabled, enabled it again. Otherwise,
     * just wait for the distributor to have digested our changes.
     */
    if (enabled)
        gic_unmask_irq(d);
    else
        gic_dist_wait_for_rwp();

    irq_data_update_effective_affinity(d, cpumask_of(cpu));

    return IRQ_SET_MASK_OK_DONE;
}

static struct irq_chip gic_chip = {
    .name			= "GICv3",
    .irq_mask		= gic_mask_irq,
    .irq_unmask		= gic_unmask_irq,
    .irq_eoi		= gic_eoi_irq,
    .irq_set_type		= gic_set_type,
    .irq_set_affinity	= gic_set_affinity,
    .irq_get_irqchip_state	= gic_irq_get_irqchip_state,
    .irq_set_irqchip_state	= gic_irq_set_irqchip_state,
    .flags			= IRQCHIP_SET_TYPE_MASKED |
                  IRQCHIP_SKIP_SET_WAKE |
                  IRQCHIP_MASK_ON_SUSPEND,
};

static struct irq_chip gic_eoimode1_chip = {
    .name			= "GICv3",
    .irq_mask		= gic_eoimode1_mask_irq,
    .irq_unmask		= gic_unmask_irq,
    .irq_eoi		= gic_eoimode1_eoi_irq,
    .irq_set_type		= gic_set_type,
    .irq_set_affinity	= gic_set_affinity,
    .irq_get_irqchip_state	= gic_irq_get_irqchip_state,
    .irq_set_irqchip_state	= gic_irq_set_irqchip_state,
    .flags			= IRQCHIP_SET_TYPE_MASKED |
                  IRQCHIP_SKIP_SET_WAKE |
                  IRQCHIP_MASK_ON_SUSPEND,
};

#define GIC_ID_NR	(1U << GICD_TYPER_ID_BITS(gic_data.rdists.gicd_typer))

static int gic_irq_domain_map(struct irq_domain *d, int irq,
                  irq_hw_number_t hw)
{
    struct irq_chip *chip = &gic_chip;

    if (likely(supports_deactivate_key))
        chip = &gic_eoimode1_chip;

    /* SGIs are private to the core kernel */
    if (hw < 16)
        return -EPERM;
    /* Nothing here */
    if (hw >= gic_data.irq_nr && hw < 8192)
        return -EPERM;
    /* Off limits */
    if (hw >= GIC_ID_NR)
        return -EPERM;

    /* PPIs */
    if (hw < 32) {
        irq_set_percpu_devid(irq);
        irq_domain_set_info(d, irq, hw, chip, d->host_data,
                    handle_percpu_devid_irq, NULL, NULL);
    }
    /* SPIs */
    if (hw >= 32 && hw < gic_data.irq_nr) {
        irq_domain_set_info(d, irq, hw, chip, d->host_data,
                    handle_fasteoi_irq, NULL, NULL);
    }
    /* LPIs */
    if (hw >= 8192 && hw < GIC_ID_NR) {
        if (!gic_dist_supports_lpis())
            return -EPERM;
        irq_domain_set_info(d, irq, hw, chip, d->host_data,
                    handle_fasteoi_irq, NULL, NULL);
    }

    return 0;
}

#define GIC_IRQ_TYPE_PARTITION	(GIC_IRQ_TYPE_LPI + 1)

static int gic_irq_domain_translate(struct irq_domain *d,
                    struct irq_fwspec *fwspec,
                    unsigned long *hwirq,
                    unsigned int *type)
{
    if (is_of_node(fwspec->fwnode)) {
        if (fwspec->param_count < 3)
            return -EINVAL;

        switch (fwspec->param[0]) {
        case 0:			/* SPI */
            *hwirq = fwspec->param[1] + 32;
            break;
        case 1:			/* PPI */
        case GIC_IRQ_TYPE_PARTITION:
            *hwirq = fwspec->param[1] + 16;
            break;
        case GIC_IRQ_TYPE_LPI:	/* LPI */
            *hwirq = fwspec->param[1];
            break;
        default:
            return -EINVAL;
        }

        *type = fwspec->param[2] & IRQ_TYPE_SENSE_MASK;

        /*
         * Make it clear that broken DTs are... broken.
         * Partitionned PPIs are an unfortunate exception.
         */
        WARN_ON(*type == IRQ_TYPE_NONE &&
            fwspec->param[0] != GIC_IRQ_TYPE_PARTITION);
        return 0;
    }

    if (is_fwnode_irqchip(fwspec->fwnode)) {
        if(fwspec->param_count != 2)
            return -EINVAL;

        *hwirq = fwspec->param[0];
        *type = fwspec->param[1];

        WARN_ON(*type == IRQ_TYPE_NONE);
        return 0;
    }

    return -EINVAL;
}

static int gic_irq_domain_alloc(struct irq_domain *domain, int virq,
                int nr_irqs, void *arg)
{
    int i, ret;
    irq_hw_number_t hwirq;
    unsigned int type = IRQ_TYPE_NONE;
    struct irq_fwspec *fwspec = arg;

    ret = gic_irq_domain_translate(domain, fwspec, &hwirq, &type);
    if (ret)
        return ret;

    for (i = 0; i < nr_irqs; i++) {
        ret = gic_irq_domain_map(domain, virq + i, hwirq + i);
        if (ret)
            return ret;
    }

    return 0;
}

static void gic_irq_domain_free(struct irq_domain *domain, int virq,
                int nr_irqs)
{
    int i;

    for (i = 0; i < nr_irqs; i++) {
        struct irq_data *d = irq_domain_get_irq_data(domain, virq + i);
        irq_set_handler(virq + i, NULL);
        irq_domain_reset_irq_data(d);
    }
}

static int gic_irq_domain_select(struct irq_domain *d,
                 struct irq_fwspec *fwspec,
                 enum irq_domain_bus_token bus_token)
{
    /* Not for us */
        if (fwspec->fwnode != d->fwnode)
        return 0;

    /* If this is not DT, then we have a single domain */
    if (!is_of_node(fwspec->fwnode))
        return 1;

    /*
     * If this is a PPI and we have a 4th (non-null) parameter,
     * then we need to match the partition domain.
     */
    if (fwspec->param_count >= 4 &&
        fwspec->param[0] == 1 && fwspec->param[3] != 0) {
        pr_warn("select domain, Current not supprot partition\n");
        return false;
    }

    return d == gic_data.domain;
}

static const struct irq_domain_ops gic_irq_domain_ops = {
    .translate = gic_irq_domain_translate,
    .alloc = gic_irq_domain_alloc,
    .free = gic_irq_domain_free,
    .select = gic_irq_domain_select,
};

static bool gic_enable_quirk_msm8996(void *data)
{
    struct gic_chip_data *d = data;

    d->flags |= FLAGS_WORKAROUND_GICR_WAKER_MSM8996;

    return true;
}

static int __init gic_init_bases(void __iomem *dist_base,
                 struct redist_region *rdist_regs,
                 u32 nr_redist_regions,
                 u64 redist_stride,
                 struct fwnode_handle *handle)
{
    u32 typer;
    int gic_irqs;
    int err;

    if (!is_boot_el2()) {
        supports_deactivate_key = false;
    }

    if (likely(supports_deactivate_key)) {
        pr_info("GIC: Using split EOI/Deactivate mode\n");
    }

    gic_data.fwnode = handle;
    gic_data.dist_base = dist_base;
    gic_data.redist_regions = rdist_regs;
    gic_data.nr_redist_regions = nr_redist_regions;
    gic_data.redist_stride = redist_stride;

    /*
     * Find out how many interrupts are supported.
     * The GIC only supports up to 1020 interrupt sources (SGI+PPI+SPI)
     */
    typer = readl_relaxed(gic_data.dist_base + GICD_TYPER);
    gic_data.rdists.gicd_typer = typer;
    gic_irqs = GICD_TYPER_IRQS(typer);
    if (gic_irqs > 1020)
        gic_irqs = 1020;
    gic_data.irq_nr = gic_irqs;

    gic_data.domain = irq_domain_create_tree(handle, &gic_irq_domain_ops, HWIRQ_MAX,
                         &gic_data);
    irq_domain_update_bus_token(gic_data.domain, DOMAIN_BUS_WIRED);
    gic_data.rdists.rdist = &rdist_percpu;
    gic_data.rdists.has_vlpis = true;
    gic_data.rdists.has_direct_lpi = true;

    if (WARN_ON(!gic_data.domain) || WARN_ON(!gic_data.rdists.rdist)) {
        err = -ENOMEM;
        goto out_free;
    }

    gic_data.has_rss = !!(typer & GICD_TYPER_RSS);
    pr_info("Distributor has %sRange Selector support\n",
        gic_data.has_rss ? "" : "no ");

    if (typer & GICD_TYPER_MBIS) {
        pr_warn("Not support to MBIs\n");
    }

    set_handle_irq(gic_handle_irq);

    gic_update_vlpi_properties();

    gic_smp_init();
    gic_dist_init();
    gic_cpu_init();

    return 0;
out_free:
    if (gic_data.domain)
        irq_domain_remove(gic_data.domain);

    return err;
}

static int __init gic_validate_dist_version(void __iomem *dist_base)
{
    u32 reg = readl_relaxed(dist_base + GICD_PIDR2) & GIC_PIDR2_ARCH_MASK;

    if (reg != GIC_PIDR2_ARCH_GICv3 && reg != GIC_PIDR2_ARCH_GICv4)
        return -ENODEV;

    return 0;
}

/* Create all possible partitions at boot time */
static void __init gic_populate_ppi_partitions(struct device_node *gic_node)
{
    struct device_node *parts_node;

    parts_node = of_get_child_by_name(gic_node, "ppi-partitions");
    if (parts_node)
        pr_warn("Current not support ppi-partitions\n");
}

static void __init gic_of_setup_kvm_info(struct device_node *node)
{
    int ret;
    struct resource r;
    u32 gicv_idx;

    gic_v3_kvm_info.type = GIC_V3;

    gic_v3_kvm_info.maint_irq = irq_of_parse_and_map(node, 0);
    if (!gic_v3_kvm_info.maint_irq)
        return;

    if (of_property_read_u32(node, "#redistributor-regions",
                 &gicv_idx))
        gicv_idx = 1;

    gicv_idx += 3;	/* Also skip GICD, GICC, GICH */
    ret = of_address_to_resource(node, gicv_idx, &r);
    if (!ret)
        gic_v3_kvm_info.vcpu = r;

    gic_v3_kvm_info.has_v4 = gic_data.rdists.has_vlpis;
    gic_set_kvm_info(&gic_v3_kvm_info);
}

static const struct gic_quirk gic_quirks[] = {
    {
        .desc	= "GICv3: Qualcomm MSM8996 broken firmware",
        .compatible = "qcom,msm8996-gic-v3",
        .init	= gic_enable_quirk_msm8996,
    },
    {
    }
};

static int __init gic_of_init(struct device_node *node, struct device_node *parent)
{
    void __iomem *dist_base;
    struct redist_region *rdist_regs;
    u64 redist_stride;
    u32 nr_redist_regions;
    int err, i;

    dist_base = of_iomap(node, 0);
    if (!dist_base) {
        pr_err("%pOF: unable to map gic dist registers\n", node);
        return -ENXIO;
    }

    err = gic_validate_dist_version(dist_base);
    if (err) {
        pr_err("%pOF: no distributor detected, giving up\n", node);
        goto out_unmap_dist;
    }

    if (of_property_read_u32(node, "#redistributor-regions", &nr_redist_regions))
        nr_redist_regions = 1;

    rdist_regs = kcalloc(nr_redist_regions, sizeof(*rdist_regs),
                 GFP_KERNEL);
    if (!rdist_regs) {
        err = -ENOMEM;
        goto out_unmap_dist;
    }

    for (i = 0; i < (int)nr_redist_regions; i++) {
        struct resource res;
        int ret;

        ret = of_address_to_resource(node, 1 + i, &res);
        rdist_regs[i].redist_base = of_iomap(node, 1 + i);
        if (ret || !rdist_regs[i].redist_base) {
            pr_err("%pOF: couldn't map region %d\n", node, i);
            err = -ENODEV;
            goto out_unmap_rdist;
        }
        rdist_regs[i].phys_base = res.start;
    }

    if (of_property_read_u64(node, "redistributor-stride", &redist_stride))
        redist_stride = 0;

    gic_enable_of_quirks(node, gic_quirks, &gic_data);

    err = gic_init_bases(dist_base, rdist_regs, nr_redist_regions,
                 redist_stride, &node->fwnode);
    if (err)
        goto out_unmap_rdist;

    gic_populate_ppi_partitions(node);

    if (likely(supports_deactivate_key))
        gic_of_setup_kvm_info(node);
    return 0;

out_unmap_rdist:
out_unmap_dist:
    BUG();
    return err;

}
IRQCHIP_DECLARE(gic_v3, "arm,gic-v3", gic_of_init);
