// SPDX-License-Identifier: (GPL-2.0 OR MPL-1.1)
/*======================================================================

    A driver for PCMCIA serial devices

    serial_cs.c 1.134 2002/05/04 05:48:53

    The contents of this file are subject to the Mozilla Public
    License Version 1.1 (the "License"); you may not use this file
    except in compliance with the License. You may obtain a copy of
    the License at http://www.mozilla.org/MPL/

    Software distributed under the License is distributed on an "AS
    IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
    implied. See the License for the specific language governing
    rights and limitations under the License.

    The initial developer of the original code is David A. Hinds
    <dahinds@users.sourceforge.net>.  Portions created by David A. Hinds
    are Copyright (C) 1999 David A. Hinds.  All Rights Reserved.

    Alternatively, the contents of this file may be used under the
    terms of the GNU General Public License version 2 (the "GPL"), in which
    case the provisions of the GPL are applicable instead of the
    above.  If you wish to allow the use of your version of this file
    only under the terms of the GPL and not to allow others to use
    your version of this file under the MPL, indicate your decision
    by deleting the provisions above and replace them with the notice
    and other provisions required by the GPL.  If you do not delete
    the provisions above, a recipient may use your version of this
    file under either the MPL or the GPL.

======================================================================*/
#include <seminix/of.h>
#include <seminix/of_fdt.h>
#include <seminix/spinlock.h>
#include <devices/serial.h>
#include <asm/fixmap.h>

struct console_device console_dev = {
    .lock = __SPIN_LOCK_UNLOCKED(console_dev.lock),
    .available = 0,
};

void console_write(const char *s, unsigned int count)
{
    unsigned long flags;

    spin_lock_irqsave(&console_dev.lock, flags);
    if (likely(console_dev.available))
        console_dev.write(&console_dev, s, count);
    spin_unlock_irqrestore(&console_dev.lock, flags);
}

bool console_device_available(void)
{
    return console_dev.available;
}

static void __init console_init(struct console_device *device,
                 const char *name)
{
    strlcpy(device->name, name, min(strlen(name) + 1, sizeof(device->name)));

    if (device->iotype == SERIAL_IO_MEM || device->iotype == SERIAL_IO_MEM16 ||
        device->iotype == SERIAL_IO_MEM32 || device->iotype == SERIAL_IO_MEM32BE)
        pr_info("%s at MMIO%s %pa (options '%s')\n",
            device->name,
            (device->iotype == SERIAL_IO_MEM) ? "" :
            (device->iotype == SERIAL_IO_MEM16) ? "16" :
            (device->iotype == SERIAL_IO_MEM32) ? "32" : "32be",
            &device->mapbase, device->options);
    else
        pr_info("%s at I/O port 0x%lx (options '%s')\n",
            device->name,
            device->iobase, device->options);
}

int __init of_setup_console(const struct console_id *match,
    unsigned long node, const char *options)
{
    int err;
    const __be32 *val;
    bool big_endian;
    u64 addr;

    console_dev.iotype = SERIAL_IO_MEM;
    addr = of_flat_dt_translate_address(node);
    if (addr == OF_BAD_ADDR) {
        pr_warn("[%s] bad address\n", match->name);
        return -ENXIO;
    }
    console_dev.mapbase = addr;

    val = of_get_flat_dt_prop(node, "reg-offset", NULL);
    if (val)
        console_dev.mapbase += be32_to_cpu(*val);
    console_dev.membase = __fixmap_remap_console(console_dev.mapbase, FIXMAP_PAGE_IO);

    val = of_get_flat_dt_prop(node, "reg-shift", NULL);
    if (val)
        console_dev.regshift = be32_to_cpu(*val);
    big_endian = of_get_flat_dt_prop(node, "big-endian", NULL) != NULL ||
        (IS_ENABLED(CONFIG_CPU_BIG_ENDIAN) &&
         of_get_flat_dt_prop(node, "native-endian", NULL) != NULL);
    val = of_get_flat_dt_prop(node, "reg-io-width", NULL);
    if (val) {
        switch (be32_to_cpu(*val)) {
        case 1:
            console_dev.iotype = SERIAL_IO_MEM;
            break;
        case 2:
            console_dev.iotype = SERIAL_IO_MEM16;
            break;
        case 4:
            console_dev.iotype = (big_endian) ? SERIAL_IO_MEM32BE : SERIAL_IO_MEM32;
            break;
        default:
            pr_warn("[%s] unsupported reg-io-width\n", match->name);
            return -EINVAL;
        }
    }

    val = of_get_flat_dt_prop(node, "current-speed", NULL);
    if (val)
        console_dev.baud = be32_to_cpu(*val);

    val = of_get_flat_dt_prop(node, "clock-frequency", NULL);
    if (val)
        console_dev.clk = be32_to_cpu(*val);

    if (options) {
        console_dev.baud = simple_strtoul(options, NULL, 0);
        strlcpy(console_dev.options, options,
            sizeof(console_dev.options));
    }
    console_init(&console_dev, match->name);
    strlcpy(console_dev.compatible, match->compatible, sizeof (console_dev.compatible));
    err = match->setup(&console_dev, options);
    if (err < 0)
        return err;
    if (!console_dev.write)
        return -ENODEV;

    console_dev.available = 1;

    return 0;
}
