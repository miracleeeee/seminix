# SPDX-License-Identifier: Apache-2.0

cmake_minimum_required(VERSION 3.20.0)

find_package(musllibc REQUIRED)
find_package(util_libs REQUIRED)

include(${CMAKE_CURRENT_LIST_DIR}/cmake/app/boilerplate.cmake)

musllibc_import_library()
util_libs_import_library()

target_link_libraries(kernel_interface INTERFACE musllibc_interface)

add_subdirectory(src)
add_subdirectory(libseminix)
